const electron = require('electron');
const app = electron.app;
const redigeFileService = require('./back/RedigeFileService');
const updater = require('./integrations/updater');
const log = require('electron-log');
const macosSetup = require('./integrations/macosSetup');
const unixSetup = require('./integrations/unixSetup');
const windowsSetup = require('./integrations/windowsSetup');
const mainWindow = require('./mainWindow/mainWindow');
const homeWindow = require('./home/homeWindow');
const command = require('./command/command');
const openFileService = require('./back/OpenFileService');
const cache = require('./back/cache');


process.on('uncaughtException', function (error) {
    log.error(error);
});

/*
 *  Suppose by default that we want to open the GUI (so not headless).
 *  This(could be prevent cf windowsSetup)
 */
app.headless = command.headless;

app.setName('Redige');

macosSetup.setup();
unixSetup.setup();
windowsSetup.setup();

/*
 * Used for macOs / win updater.
 * Check and download the new version if any, the next restart will use the new version.
 */
updater.checkForUpdates();

if (!app.headless) {
    app.on('ready', function () {

        cache.loadCache();

        //Init back
        redigeFileService.init();

        const fileToOpen = openFileService.getRDGFromArgs(command.path);
        if (!fileToOpen) {
            openFileService.getRecentFiles().then(files => {
                homeWindow.initWindow(files.length === 0);
            });
        } else {
            openFileService.doesFileExists(fileToOpen).then(() => {
                mainWindow.initWindow(fileToOpen);
            }).catch(e => {
                console.error(e);
            })
        }
    });
}
