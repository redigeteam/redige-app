angular.module('redige-app').directive('fileExplorer', function ($rootScope) {
    'use strict';
    return {
        restrict: 'E',
        scope: false,
        templateUrl: './fileExplorer/fileExplorer.html',
        link: function (scope) {
            scope.openNoDialog = window.tabs.openNoDialog;
            scope.sortedPaths;
            scope.explodedFiles;

            $rootScope.$watch(function () {
                return window.tabs.explodedFiles;
            }, value => {
                if (value) {
                    scope.explodedFiles = value;
                    scope.sortedPaths = sortPaths(scope.sortedPaths);
                }
            });

            function sortPaths(rootNode) {
                if (rootNode === undefined) {
                    const explodedRoot = scope.explodedFiles[0].path;
                    rootNode = { path: explodedRoot, label: explodedRoot.replace(/^.*[\\\/]/, ''), children: [] };
                    scope.explodedFiles.shift(); // remove root folder from file list
                    scope.explodedFiles.forEach(file => {
                        insertFileInTree(rootNode, file);
                    });
                }
                return rootNode;
            }

            function insertFileInTree(root, file) {
                // limit the number of sub folders for AngularJS
                if ((file.path.replace(root.path, '').match(/\//g) || []).length <= 5) {
                    let fileName = file.path.replace(/^.*[\\\/]/, '');
                    let fileFolder = file.path.replace('/' + fileName, '');

                    if (root.path === fileFolder) {
                        root.children.push({
                            label: fileName,
                            path: file.path,
                            isDirectory: file.isDirectory,
                            children: []
                        });
                    } else if (!root.path.endsWith('.md')) {
                        root.children.forEach(newRoot => {
                            insertFileInTree(newRoot, file);
                        });
                    }
                }
            }
        }
    };
});
