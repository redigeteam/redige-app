process.once('loaded', () => {
    const electronApp = require('electron');
    const log = require('electron-log');
    const _ = require('lodash');
    const ipcRenderer = electronApp.ipcRenderer;
    const config = require('../config');
    const redigeFileService = electronApp.remote.require('./back/RedigeFileService');
    const saveService = electronApp.remote.require('./back/SaveFileService');
    const spellingService = require('../back/SpellingService');

    const errorFn = console.error;
    console.error = function () {
        errorFn.apply(console, arguments);
        ipcRenderer.sendToHost('error-event', arguments);
    };
    window.onerror = function () {
        ipcRenderer.sendToHost('error-event', [arguments]);
    };

    window.async = {};
    window.electron = {
        userCustomCSS: config.redigeHomePath + "/custom.css",
        rdgParentPath: null,
        workingDirectory: null,
        setDirty: function () {
            ipcRenderer.sendToHost('rdg-dirty-event');
        },
        openNoDialog: function (path) {
            ipcRenderer.sendToHost('rdg-open-no-dialog-event', path, Redige.getDocument().template);
        },
        save: function () {
            const doc = Redige.parse();
            if (doc.file !== undefined) {
                const selection = Redige.getDocumentSelection();
                doc.file.selection = {
                    startSectionId: selection.startSection.id,
                    startOffset: selection.startOffset,
                    endSectionId: selection.endSection.id,
                    endOffset: selection.endOffset,
                };

                doc.file.parentPath = window.electron.rdgParentPath;
                doc.file.workingDirectory = window.electron.workingDirectory;
            }

            saveService.save(doc, (err, fileDescriptor) => {
                    if (fileDescriptor) {
                        Redige.getDocument().file = fileDescriptor;
                        ipcRenderer.sendToHost('rdg-title-event', fileDescriptor);
                    }
                }
            );
        },
        toPDF: function () {
            ipcRenderer.sendToHost('rdg-pdf-event', Redige.parse());
        },
        publish: function () {
            redigeFileService.publish(Redige.parse());
        },
        exportReveal: function (revealFolder) {
            if (!accessingParentFolder(revealFolder)) {
                ipcRenderer.sendToHost('rdg-reveal-event', Redige.parse(), revealFolder);
            }
        },
        exportRevealPdf: function (revealFolder) {
            if (!accessingParentFolder(revealFolder)) {
                ipcRenderer.sendToHost('rdg-reveal-pdf-event', Redige.parse(), revealFolder);
            }
        },
        exportEpub: function () {
            ipcRenderer.sendToHost('rdg-epub-event', Redige.parse());
        }
    };

    function accessingParentFolder(path) {
        const matches = path.match(/\.\./i) !== null;
        if (matches) {
            log.error('Path provided to export function is trying to access parent folders !');
        }
        return matches;
    }

    document.addEventListener("rdg-ready-event", function () {
        ipcRenderer.sendToHost('rdg-ready-event');
    });

    document.addEventListener("rdg-fileInserted-event", function (event) {
        ipcRenderer.sendToHost('rdg-fileInserted-event', event.fileId, event.filePath, Redige.getDocument().file.workingDirectory);
        window.electron.setDirty();
    });

    document.addEventListener('dragover', event => {
        event.preventDefault();
        return true;
    }, false);

    document.addEventListener('drop', event => {
        event.preventDefault();
        return true;
    }, false);

    document.addEventListener('keyup', _.debounce(() => {
        const section = Redige.getDocumentSelection().startSection;
        setTimeout(() => {
            if (spellingService.shouldCheck(section)) {
                spellingService.checkSection(section).then(() => {
                    Redige.restoreSelection();
                });
            }
        });
    }, 800));

    document.addEventListener('keydown', (event) => {
        const sectionToCheck = Redige.getDocumentSelection().startSection;
        if (event.which === 13 && spellingService.shouldCheck(sectionToCheck)) {
            setTimeout(() => {
                spellingService.checkSection(sectionToCheck);
            }, 100);
        }
    });

    document.addEventListener('contextmenu', (event) => {
        const selection = Redige.getDocumentSelection();
        const spellingDecorator = selection.startSection.decorators.find(decorator => {
            return (decorator.type === 'spelling-error' || decorator.type === 'grammar-error') &&
                decorator.start <= selection.startOffset &&
                decorator.end >= selection.endOffset;
        });
        if (spellingDecorator) {
            ipcRenderer.sendToHost('rdg-spell-suggest', spellingDecorator, _.pick(selection.startSection, ['content', 'id', 'decorators']));
        }
    });

    document.addEventListener('mousedown', () => {
        const selection = Redige.getDocumentSelection();
        if (selection !== undefined && spellingService.shouldCheck(selection.startSection)) {
            spellingService.checkSection(selection.startSection);
        }
    });

    document.addEventListener('paste', () => {
        window.electron.setDirty();
    });

    ipcRenderer.on('updateWord', (event, word, section, decorator) => {
        let selection = {
            startSection: section,
            endSection: section,
            startOffset: decorator.start,
            endOffset: decorator.end
        };
        Redige.insertInSection({ content: word, decorators: [] }, selection).then((updatedSection) => {
            return spellingService.checkSection(updatedSection).then(() => {
                Redige.setSelection(updatedSection.id, decorator.start + word.length);
                Redige._editor.focus();
            });
        });
    });

    ipcRenderer.on('setDocument', (event, newDocument) => {
        _.merge(newDocument.template, require(newDocument.template.path));
        window.electron.templateCSS = newDocument.template.cssPath;

        if (newDocument.file !== undefined) {
            window.electron.rdgParentPath = newDocument.file.parentPath;
            window.electron.workingDirectory = newDocument.file.workingDirectory;
        }

        Redige.patch({ action: 'load', data: newDocument }).then(function () {
            spellingService.checkDocument(Redige.getDocument());
            const lastSection = Redige.getDocument().querySelector(".section:last-child");
            if (newDocument.file !== undefined && newDocument.file.selection !== undefined) {
                const cachedSelection = newDocument.file.selection;
                Redige.setSelection(cachedSelection.startSectionId, cachedSelection.startOffset, cachedSelection.endSectionId, cachedSelection.endOffset);
            } else if (lastSection) {
                Redige.setSelection(lastSection.id, 0);
            }
            ipcRenderer.sendToHost('rdg-title-event', newDocument.file);
            ipcRenderer.sendToHost('rdg-document-set');
            setTimeout(() => {
                Redige._editor.focus();
            }, 1000);
        });
    });

    ipcRenderer.on('setFile', (event, fileId, filePath) => {
        Redige.patch({ action: 'updateSection', section: { id: fileId, data: filePath } })
    });

    ipcRenderer.on('setLoader', (event, isLoaderActive) => {
        document.dispatchEvent(new CustomEvent('setLoader', { detail: { isActive: isLoaderActive } }));
    });

    document.addEventListener('structureChanged', () => {
        const document = Redige.parse();

        const titles = document.children.filter(section => {
            return section.level > 0 && section.type === 'hierarchical';
        }).map(title => {
            return { id: title.id, text: title.content, level: title.level, numbering: title.numbering }
        });

        const fileData = { path: '', titles: titles };
        if (document.file !== undefined) {
            fileData.path = document.file.path;
        }

        ipcRenderer.sendToHost('rdg-titles-event', fileData);
    });

    ipcRenderer.on('setSidePanelActive', (event, isActive) => {
        const cssClass = isActive ? 'sidepanelActive' : 'sidepanelNotActive';
        const editor = document.getElementById('editor');
        editor.classList.remove('sidepanelActive', 'sidepanelNotActive');
        editor.classList.add(cssClass);
    });

    ipcRenderer.on('scrollToTitle', (event, title) => {
        document.dispatchEvent(new CustomEvent('scrollToTitle', { detail: { title: title } }));
    });

    ipcRenderer.on('setSelection', (event, selection) => {
        Redige.setSelection(selection.startSectionId, selection.startOffset, selection.endSectionId, selection.endOffset);
    });
});
