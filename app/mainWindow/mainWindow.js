const windowStateKeeper = require('electron-window-state');
const { BrowserWindow, ipcMain } = require('electron');
const path = require('path');
const appVersion = require('../package.json').version;
const config = require('../config');
const home = require('../home/homeWindow');

const windowReferences = {};

function initNewWindow(mainWindowState, documentPath) {
    const newWindow = getNewBrowserWindow(mainWindowState);
    windowReferences[newWindow.id] = newWindow;
    setDocumentAndEvents(newWindow.id, documentPath);
    return newWindow.id;
}

function getNewBrowserWindow(mainWindowState) {
    let iconPath;
    if (process.platform === 'linux') {
        iconPath = path.join(__dirname, '..', 'icon.png');
    }

    const newWindow = new BrowserWindow({
        'width': mainWindowState.width,
        'height': mainWindowState.height,
        title: 'Rédige ' + appVersion,
        icon: iconPath,
        show: false,
        useContentSize: true,
        webPreferences: {
            nodeIntegration: true,
            devTools: config.env === 'DEV'
        }
    });

    newWindow.loadURL('file://' + __dirname + '/electron-tabs.html');

    return newWindow;
}

function setDocumentAndEvents(windowId, documentPath) {
    ipcMain.once('get-sender', ipcEvent => {

        ipcEvent.sender.send('set-document-new-window', documentPath, windowId);

        windowReferences[windowId].on('close', e => {
            e.preventDefault();
            ipcEvent.sender.send('closing-window', windowId);
        });
    });

    ipcMain.on('closing-ok', (event, id) => {
        if (windowReferences[id]) {
            windowReferences[id].destroy();
            delete windowReferences[id];
        }
    });
    
    ipcMain.on('crashing', (event, id) => {
        if (windowReferences[id]) {
            if (Object.keys(windowReferences).length === 1) {
                home.initWindow(false);
            }
            windowReferences[id].destroy();
            delete windowReferences[id];
        }
    });

    ipcMain.on('ready-to-show-custom', (event, id) => {
        windowReferences[id].show();
        windowReferences[id].focus();
    });
}

module.exports = {
    initWindow: function (documentPath) {
        // Load the previous state with fallback to defaults
        const mainWindowState = windowStateKeeper({
            defaultWidth: 1000,
            defaultHeight: 800
        });

        const firstWindowId = initNewWindow(mainWindowState, documentPath);
        mainWindowState.manage(windowReferences[firstWindowId]);

        ipcMain.on('error-event', (event, err) => {
            console.error.apply(console, err);
        });
    }
};
