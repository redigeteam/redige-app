angular.module('redige-app').directive('sidePanel', function () {
    'use strict';
    return {
        restrict: 'E',
        scope: false,
        templateUrl: './sidePanel/sidePanel.html',
        link: function (scope) {
            scope.isSummaryActive = false;
            scope.isFileExplorerActive = false;

            scope.toggleFileExplorer = function () {
                const oldState = scope.isFileExplorerActive;
                resetPanels();
                scope.isFileExplorerActive = !oldState;
                notifyWebview();
            };
            scope.toggleSummary = function () {
                const oldState = scope.isSummaryActive;
                resetPanels();
                scope.isSummaryActive = !oldState;
                notifyWebview();
            };

            function resetPanels() {
                scope.isFileExplorerActive = false;
                scope.isSummaryActive = false;
            }

            function notifyWebview() {
                const isActive = scope.isSummaryActive || scope.isFileExplorerActive;
                document.dispatchEvent(new CustomEvent('setSidePanelActive', { detail: { isActive: isActive } }));
            }
        }
    };
});
