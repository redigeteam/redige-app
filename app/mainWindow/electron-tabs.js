const dragula = require("dragula");
const TabGroup = require("electron-tabs");
const log = require('electron-log');
const ipcRenderer = require('electron').ipcRenderer;
const { remote } = require('electron');
const electronDialog = remote.dialog;
const BrowserWindow = remote.BrowserWindow;
const version = require('../package.json').version;
const config = require('../config');
const cache = remote.require('./back/cache');
const redigeFileService = remote.require('./back/RedigeFileService');
const openFileService = remote.require('./back/OpenFileService');
const fs = require('bluebird').promisifyAll(require('fs'));
const url = require('url');
const path = require('path');

const { Menu, MenuItem } = remote;
let wordMenu = new Menu();

let currentWindowId;
let currentTemplate;
let currentDocumentPath;
let currentWorkingDirectory;
let panelActive = false;

window.tabs = {
    openNoDialog: onOpenNoDialog
};

const errorFn = console.error;
console.error = function () {
    errorFn.apply(console, arguments);
    ipcRenderer.send('error-event', arguments);
};

window.onerror = function () {
    ipcRenderer.send('error-event', [arguments]);
};

function isJavaInstalled() {
    return new Promise((resolve, reject) => {
        const spawn = require('child_process').spawn('java', ['-version']);
        spawn.on('error', err => {
            reject(err);
        });
        spawn.stderr.on('data', data => {
            data = data.toString().split('\n')[0];
            new RegExp('version').test(data) ? resolve() : reject();
        });
    });
}

let tabGroup = new TabGroup({
    ready: function (tabGroup) {
        dragula([tabGroup.tabContainer], {
            direction: "horizontal"
        });
    }
});

tabGroup.on("tab-active", tab => {
    updateTitle(tab);
});

function createNewTab(title) {
    const tabTitle = (title || 'New file');

    let newTab = tabGroup.addTab({
        title: tabTitle,
        src: 'file://' + __dirname + '/../front/app/index.html',
        visible: true,
        webviewAttributes: {
            nodeIntegration: false,
            preload: 'file://' + __dirname + '/inject.js'
        }
    });

    newTab.on('closing', onClosing);
    newTab.webview.addEventListener('dom-ready', onTabReady.bind(this, newTab));
    newTab.webview.addEventListener('ipc-message', onMessage.bind(this, newTab));

    newTab.webview.addEventListener('new-window', (event) => {
        event.preventDefault();
        redigeFileService.openBrowser(event.url);
    });

    newTab.isDirty = false;
    newTab.activate();
    return newTab;
}

function onMessage(tab, event) {
    if (event.channel === 'rdg-open-no-dialog-event') {
        onOpenNoDialog(event.args[0], event.args[1]);
    } else if (event.channel === 'rdg-ready-event') {
        onRdgReady(tab);
    } else if (event.channel === 'rdg-dirty-event') {
        updateTitle(tab, true);
    } else if (event.channel === 'rdg-title-event') {
        tab.fileData = event.args[0];
        updateTitle(tab, false);
    } else if (event.channel === 'rdg-fileInserted-event') {
        onFileInserted(tab, event.args[0], event.args[1], event.args[2]);
    } else if (event.channel === 'rdg-pdf-event') {
        onPdf(event.args[0]);
    } else if (event.channel === 'rdg-titles-event') {
        onTitles(event.args[0]);
    } else if (event.channel === 'error-event') {
        ipcRenderer.send('error-event', event.args);
    } else if (event.channel === 'rdg-spell-suggest') {
        event.args.unshift(tab);
        onSpellSuggest.apply(this, event.args);
    } else if (event.channel === 'rdg-reveal-event') {
        onReveal(event.args[0], event.args[1]);
    } else if (event.channel === 'rdg-reveal-pdf-event') {
        onRevealPdf(event.args[0], event.args[1]);
    } else if (event.channel === 'rdg-epub-event') {
        onEpub(tab, event.args[0]);
    }
}

function onRdgReady(tab) {
    if (tab.document) {
        tab.webview.send("setDocument", tab.document);
        onTitles();
    }
}

function onOpenNoDialog(path, template) {
    return new Promise(resolve => {
        let openedTab = tabGroup.tabs.find(tab => {
            return tab.document.file.path === path;
        });

        if (openedTab === undefined) {
            openFileService.openNoDialog({
                filePath: path,
                parentPath: currentDocumentPath,
                storage: { workingDirectory: currentWorkingDirectory }
            }, template || currentTemplate).then(doc => {
                return initTab(doc).then(newTab => {
                    newTab.webview.addEventListener('ipc-message', event => {
                        if (event.channel === 'rdg-document-set') {
                            resolve(newTab);
                        }
                    });
                });
            });
        } else {
            openedTab.activate();
            resolve(openedTab);
        }


    });
}

function initTab(doc) {
    return new Promise(resolve => {
        if (doc) {
            let newTab = createNewTab(doc.file.base);
            newTab.document = doc;
            resolve(newTab);
        }
    });
}

function onTabReady(tab) {
    if (config.env === 'DEV') {
        tab.webview.openDevTools();
    }
    window.addEventListener('focus', () => tab.webview.focus());
    document.dispatchEvent(new CustomEvent('setSidePanelActive', { detail: { isActive: panelActive } }));
}

function onFileInserted(tab, fileId, filePath, docFolder) {
    const protocol = url.parse(filePath).protocol;
    if (protocol !== 'http:' && protocol !== 'https:') {
        if (isImageRelativeUrl(filePath)) {
            // if URL is relative, make it absolute
            filePath = path.join(docFolder, filePath);
        }

        return fs.accessAsync(filePath, fs.F_OK).then(() => {
            // if file is not in the current project folder
            if (!path.resolve(filePath).includes(docFolder)) {
                tab.webview.send("setFile", fileId, './' + path.parse(filePath).base);
                return redigeFileService.copyFile(filePath, docFolder);
            } else {
                // transform the absolute URL to a relative one
                filePath = filePath.replace(/[\/|\\]+/g, '/');
                docFolder = docFolder.replace(/[\/|\\]+/g, '/');
                const relativeUrl = '.' + filePath.replace(docFolder, '');
                tab.webview.send("setFile", fileId, relativeUrl);
            }
        });
    }
}

function onPdf(doc) {
    return isJavaInstalled().then(() => {
        return getMergedDocuments().then(mergedChildren => {
            redigeFileService.exportPDF(mergedChildren, doc.file.directory, currentWorkingDirectory, doc.template.name).catch(err => {
                log.error(err);
                electronDialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
                    type: 'error',
                    buttons: ['Close'],
                    message: 'Erreur lors de l\'export PDF.'
                });
            });
        });
    }).catch(err => {
        log.error(err);
        electronDialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
            type: 'info',
            buttons: ['OK'],
            message: 'L\'export PDF n\'est pas disponible, veuillez installer Java.'
        });
    });
}

function getMergedDocuments() {
    // get all files to insert in the PDF
    const files = window.tabs.explodedFiles.filter(file => {
        return !file.isDirectory;
    });

    // read all the files
    return Promise.map(files, file => {
        return openFileService.openNoDialog({
            filePath: file.path,
            storage: { workingDirectory: currentWorkingDirectory }
        }, null);
    }).then(fileData => {
        let allContent = [];
        fileData.forEach(file => {
            allContent = allContent.concat(file.children);
        });
        return allContent;
    }).catch(err => {
        log.error(err);
        electronDialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
            type: 'error',
            buttons: ['OK'],
            message: 'Echec de la fusion des documents.'
        });
    });
}

function onTitles(fileData) {
    if (fileData !== undefined) {
        if (window.tabs.allTitles.length === 0) {
            // if the document has no summary
            window.tabs.allTitles[0] = fileData;
        } else {
            // find titles of the current document
            const oldTitles = window.tabs.allTitles.find(fileTitles => {
                return fileTitles.path === fileData.path;
            });
            if (oldTitles) {
                // overwrite those titles
                const index = window.tabs.allTitles.indexOf(oldTitles);
                window.tabs.allTitles[index].titles = fileData.titles;
            }
        }
    }
    document.dispatchEvent(new CustomEvent('refreshTitles'));
}

function onSpellSuggest(tab, decorator, section) {
    wordMenu = new Menu();

    if (!_.isEmpty(decorator.suggestions)) {
        decorator.suggestions.forEach(word => {
            wordMenu.append(new MenuItem({
                label: word,
                click: () => {
                    tab.webview.send('updateWord', word, section, decorator);
                }
            }))
        });
        wordMenu.popup();
    } else {

        wordMenu.append(new MenuItem({
            label: "- Aucune suggestion -",
            enabled: false
        }));

        if (decorator.message) {
            wordMenu.append(new MenuItem({
                label: decorator.message,
                enabled: false
            }));
        }
        wordMenu.popup();
    }
}

function onReveal(document, revealFolder) {
    return getMergedDocuments().then(mergedChildren => {
        document.children = mergedChildren;
        redigeFileService.exportReveal(document, revealFolder).catch(err => {
            log.error(err);
            electronDialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
                type: 'error',
                buttons: ['Close'],
                message: 'Erreur lors de l\'export Reveal'
            });
        });
    });
}

function onRevealPdf(document, revealFolder) {
    return getMergedDocuments().then(mergedChildren => {
        document.children = mergedChildren;
        redigeFileService.exportRevealPdf(document, revealFolder).catch(err => {
            log.error(err);
            electronDialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
                type: 'error',
                buttons: ['OK'],
                message: 'Erreur lors de l\'export PDF Reveal'
            });
        });
    });
}

function onEpub(tab, document) {
    return getMergedDocuments().then(mergedChildren => {
        document.children = mergedChildren;
        return redigeFileService.exportEpub(document).catch(err => {
            log.error(err);
            electronDialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
                type: 'error',
                buttons: ['OK'],
                message: 'Erreur lors de l\'export EPUB'
            });
        });
    }).catch(crash)
}

function isImageRelativeUrl(path) {
    return (path.toLowerCase().match(/^(?!\/)(?![a-zA-Z])(\.\.\/)*(\.?\/)?(.)*(jpeg|jpg|gif|svg|png)$/i) !== null);
}

function updateTitle(tab, isDirty) {
    let newTitle = "";

    if (tab.fileData !== undefined) {
        const rdgFileName = path.parse(tab.fileData.parentPath).name;
        const rdgFolder = path.parse(tab.fileData.parentPath).dir;
        const currentFile = tab.fileData.path.replace(tab.fileData.workingDirectory, '.');

        newTitle += rdgFileName + ' - [' + rdgFolder + '] - ' + currentFile + ' - ';
        tab.setTitle(path.parse(tab.fileData.path).base);
    }
    newTitle += 'Rédige ' + version;

    if (isDirty !== undefined) {
        tab.isDirty = isDirty;
    }
    if (tab.isDirty) {
        newTitle += " *";
    }

    document.title = newTitle;
}

function onClosing(tab) {
    // to prevent electron-tabs from closing too fast
    tab.closable = false;

    if (tab.isDirty) {
        tab.activate();
        electronDialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
            type: 'question',
            buttons: ['Cancel', 'Close'],
            message: 'File not saved ! Close tab without saving ?'
        }, btnIndex => {
            if (btnIndex === 1) {
                tab.isDirty = false;
                tab.close();
            } else if (btnIndex === 0) {
                tab.closable = false;
            }
        });
    } else if (isLastTab()) {
        ipcRenderer.send('closing-ok', currentWindowId);
    } else {
        tab.closable = true;
    }
}

function isLastTab() {
    if (tabGroup.tabs.length === 1) {
        return true;
    }
}

ipcRenderer.send('get-sender');
ipcRenderer.on('closing-window', (event, windowId) => {
    const dirtyTabs = tabGroup.tabs.filter(tab => tab.isDirty);
    const canClose = !dirtyTabs.length > 0;
    if (!canClose) {
        dirtyTabs[0].activate();
        electronDialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
            type: 'question',
            buttons: ['Cancel', 'Close'],
            message: 'File not saved ! Close Rédige application without saving ?'
        }, btnIndex => {
            if (btnIndex === 1) {
                ipcRenderer.send('closing-ok', windowId);
            }
        });
    } else {
        ipcRenderer.send('closing-ok', windowId);
    }
});
ipcRenderer.on('set-document-new-window', (event, documentPath, windowId) => {
    currentDocumentPath = documentPath;
    currentWindowId = windowId;

    return openFileService.openRdgCallback(currentDocumentPath).then(callbackDoc => {
        currentWorkingDirectory = callbackDoc.storage.workingDirectory;

        window.tabs.explodedFiles = callbackDoc.storage.files;
        window.tabs.allTitles = callbackDoc.allTitles;
        currentTemplate = callbackDoc.template;

        const lastOpenedFiles = cache.get('lastOpenedFiles');

        let pathOfFileToDisplay;
        let selection;

        if (lastOpenedFiles !== undefined && lastOpenedFiles[callbackDoc.file.path] !== undefined) {
            if (callbackDoc.storage.type === 'exploded') {
                const parentDir = path.parse(callbackDoc.file.path).dir;
                pathOfFileToDisplay = path.join(parentDir, lastOpenedFiles[callbackDoc.file.path].path);
            } else if (callbackDoc.storage.type === 'bundled') {
                pathOfFileToDisplay = path.join(callbackDoc.storage.workingDirectory, lastOpenedFiles[callbackDoc.file.path].path);
            }

            if (lastOpenedFiles[callbackDoc.file.path].selection !== undefined) {
                selection = lastOpenedFiles[callbackDoc.file.path].selection;
            }
        } else {
            // display first file in folders
            pathOfFileToDisplay = callbackDoc.storage.files.find(file => {
                return file.path.endsWith(callbackDoc.storage.parser);
            }).path;
        }

        if (!pathOfFileToDisplay) {
            electronDialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
                type: 'error',
                buttons: ['Close'],
                message: 'Fichier invalide.'
            });
            ipcRenderer.send('crashing', windowId);
        } else {

            return openFileService.openNoDialog({
                filePath: pathOfFileToDisplay,
                parentPath: currentDocumentPath,
                storage: callbackDoc.storage
            }, currentTemplate).then(doc => {
                doc.file.selection = selection;
                initTab(doc);
                ipcRenderer.send('ready-to-show-custom', currentWindowId);
            });
        }
    }).catch((err) => {
        log.error(err);
        ipcRenderer.send('crashing', windowId);
    });
});

document.getElementsByClassName('etabs-tabgroup')[0].addEventListener('dragover', event => {
    event.preventDefault();
    return true;
}, false);

document.getElementsByClassName('etabs-tabgroup')[0].addEventListener('drop', event => {
    event.preventDefault();
    return true;
}, false);

document.addEventListener('setSidePanelActive', event => {
    tabGroup.tabs.forEach(tab => {
        panelActive = event.detail.isActive;
        tab.webview.send('setSidePanelActive', panelActive);
    })
});

document.addEventListener('scrollToTitle', event => {
    onOpenNoDialog(event.detail.filePath).then(tabToDisplay => {
        tabToDisplay.webview.send('scrollToTitle', event.detail.title);
    });
});