angular.module('redige-app').directive('summary', function () {
    'use strict';
    return {
        restrict: 'E',
        scope: false,
        templateUrl: './summary/summary.html',
        link: function (scope) {
            scope.summaryScroll = summaryScroll;

            document.addEventListener('refreshTitles', () => {
                scope.allTitles = window.tabs.allTitles;
                scope.$digest();
            });
        }
    };

    function summaryScroll(filePath, title) {
        document.dispatchEvent(new CustomEvent('scrollToTitle', { detail: { filePath: filePath, title: title } }));
    }
});
