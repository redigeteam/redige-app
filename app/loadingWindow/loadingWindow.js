const { BrowserWindow } = require('electron');
const path = require('path');
const appVersion = require('../package.json').version;
const MenuService = require('../menu/MenuService');

module.exports = {
    initWindow: function (title, message) {

        let iconPath;
        if (process.platform === 'linux') {
            iconPath = path.join(__dirname, '..', 'icon.png');
        }
        const newWindow = new BrowserWindow({
            title: 'Rédige - ' + title,
            width: 350,
            height: 210,
            icon: iconPath,
            closable: false,
            modal: true,
            useContentSize: true,
            titleBarStyle: 'hidden'
        });

        newWindow.loadURL('file://' + __dirname + '/index.html?msg=' + message);

        newWindow.on('ready-to-show', function () {
            newWindow.show();
            newWindow.focus();
        });

        newWindow.on('closed', function () {
            MenuService.enableNewOpenMenu();
        });

        MenuService.disableNewOpenMenu();

        return newWindow;
    }
};
