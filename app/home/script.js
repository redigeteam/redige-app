const electronApp = require('electron');
const fs = require('fs');
const _ = require('lodash');
const openFileService = electronApp.remote.require('./back/OpenFileService');
const saveFileService = electronApp.remote.require('./back/SaveFileService');
const mainWindow = electronApp.remote.require('./mainWindow/mainWindow');
const templateService = electronApp.remote.require('./back/TemplateService');
const cache = electronApp.remote.require('./back/cache');
const config = electronApp.remote.require('./config');
const dialogs = require('dialogs');
const { remote } = require('electron');
const electronDialog = remote.dialog;
const BrowserWindow = remote.BrowserWindow;
const sanitize = require('sanitize-filename');
const unusedFileName = require('unused-filename');
const path = require('path');

angular.module('home-app', ['ui.bootstrap']).run(function ($rootScope, $timeout) {

    if (window.location.search === "?new") {
        $rootScope.activeTabIndex = 1;
    } else {
        $rootScope.activeTabIndex = 0;
    }

    $rootScope.project = {
        format: 'bundled',
        template: 'default',
        path: cache.get('defaultPath')
    };

    unusedFileName(cache.get('defaultPath') + "/Nouveau projet").then((name) => {
        $timeout(() => $rootScope.project.name = path.parse(name).name);
    });

    $rootScope.fileInput = { showMessage: false, message: 'Veuillez renseigner un dossier où créer votre projet.' };

    $rootScope.open = function () {
        openFileService.open().then(pathFile => {
            if (pathFile) {
                mainWindow.initWindow(pathFile);
                window.close();
            }
        });
    };

    $rootScope.openFromPath = function (pathFile) {
        if (pathFile) {
            cache.put('defaultPath', path.parse(pathFile).dir);
            mainWindow.initWindow(pathFile);
            window.close();
        }
    };

    $rootScope.newOther = function () {
        if ($rootScope.project.template === 'other') {
            dialogs().prompt('Veuillez entrer le code du template désiré :', '', templateCode => {
                if (templateCode !== undefined) {

                    $timeout(() => {
                        $rootScope.isLoading = true;
                    });
                    templateService.getRemoteTemplate(templateCode).then((downloadedTemplateName) => {
                        electronDialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
                            type: 'info',
                            buttons: ['OK'],
                            message: 'Template téléchargé avec succès !'
                        });
                        $timeout(() => {
                            $rootScope.isLoading = false;
                            $rootScope.templateNames = config.getTemplateNames();
                            $rootScope.project.template = downloadedTemplateName;
                        });

                    }).catch((err) => {
                        $timeout(() => {
                            $rootScope.isLoading = false;
                            $rootScope.project.template = 'default';
                        });
                        electronDialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
                            type: 'info',
                            buttons: ['OK'],
                            message: (err.downloadErrorMessage || 'Erreur dans le téléchargement de votre template.')
                        });
                        console.error(err);
                    });
                }
            });
        }
    };

    $rootScope.templateNames = config.getTemplateNames();

    openFileService.getRecentFiles().then((files) => {
        $timeout(() => {
            $rootScope.recentFiles = files.reverse();
        });
    });

    $rootScope.chooseFolder = function () {
        const chosenFolder = electronDialog.showOpenDialog({
            properties: [
                'openDirectory'
            ]
        });
        if (chosenFolder !== undefined && chosenFolder.length === 1) {
            fs.access(chosenFolder[0], fs.W_OK, err => {
                if (err) {
                    $rootScope.fileInput.message = 'Accès au dossier refusé.';
                } else {
                    $rootScope.project.path = chosenFolder[0];
                    $rootScope.$apply();
                }
            });
        }
        $rootScope.fileInput.showMessage = true;
    };

    $rootScope.submit = function (project) {
        saveFileService.createNewProject(project).then(rdgPath => {
            openFileService.addToRecents(rdgPath);
            mainWindow.initWindow(rdgPath);
            window.close();
        })
    };

    $rootScope.toNewProject = () => {
        $timeout(() => {
            const input = document.getElementById('name');
            input.setSelectionRange(0, input.value.length);
            input.focus();
        }, 100);
    }

}).filter('sanitizeFileName', function () {
    return function (value) {
        if (!_.isEmpty(value)) {
            return sanitize(value);
        } else return '';
    };
}).filter('unusedName', function () {
    return function (value, projectPath, format) {
        let fileName = path.join(projectPath, value);
        if (format === 'bundled') {
            fileName += '.rdg';
        }
        return path.parse(unusedFileName.sync(fileName)).name;
    };
});
