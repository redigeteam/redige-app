const { BrowserWindow } = require('electron');
const path = require('path');
const appVersion = require('../package.json').version;
const config = require('../config');
const MenuService = require('../menu/MenuService');

module.exports = {
    initWindow: function (shouldNewBeActive) {

        let iconPath;
        if (process.platform === 'linux') {
            iconPath = path.join(__dirname, '..', 'icon.png');
        }
        const newWindow = new BrowserWindow({
            title: 'Rédige ' + appVersion,
            width: 700,
            height: 500,
            icon: iconPath,
            useContentSize: true,
            webPreferences: {
                nodeIntegration: true,
                devTools: config.env === 'DEV',
            }
        });

        newWindow.loadURL('file://' + __dirname + '/index.html' + (shouldNewBeActive ? '?new' : ''));

        //make window visible & get the focus
        newWindow.on('ready-to-show', function () {
            newWindow.show();
            newWindow.focus();

        });

        newWindow.on('closed', function () {
            MenuService.enableNewOpenMenu();
        });

        MenuService.disableNewOpenMenu();

        return newWindow;
    }
};
