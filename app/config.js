const env = process.env.REDIGE_ENV;
const api = process.env.REDIGE_API;
const enableSpelling = process.env.REDIGE_EXPERIMENTAL_SPELLING;
const os = require('os');
const path = require('path');
const redigeHomePath = os.homedir() + '/.redige';
const templateFolder = redigeHomePath + '/templates';
const defaultTemplate = templateFolder + '/default';
const customCSSPath = redigeHomePath + '/custom.css';
const templateCssFile = 'editor.css';
const emptyDefaultTemplate = { template: { name: 'default' } };
const redigeTmp = path.join(os.tmpdir(), 'redige');
const workspaceDir = path.join(redigeTmp, 'workingDir');
const fs = require('fs');

module.exports = {
    env: env,
    api: api || "https://app.redige.net/api",
    enableSpelling,
    redigeHomePath,
    templateFolder,
    defaultTemplate,
    customCSSPath,
    templateCssFile,
    emptyDefaultTemplate,
    redigeTmp,
    workspaceDir,
    getTemplateNames: function () {
        return fs.readdirSync(templateFolder).filter(file => fs.statSync(path.join(templateFolder, file)).isDirectory());
    }
};
