const { Menu } = require('electron');

const FileMenuIndex = process.platform === 'darwin' ? 1 : 0;
const FileNewItemIndex = 0;
const FileOpenItemIndex = 1;

module.exports = {
    disableNewOpenMenu: () => {
        Menu.getApplicationMenu().items[FileMenuIndex].submenu.items[FileNewItemIndex].enabled = false;
        Menu.getApplicationMenu().items[FileMenuIndex].submenu.items[FileOpenItemIndex].enabled = false;
    },
    enableNewOpenMenu: () => {
        Menu.getApplicationMenu().items[FileMenuIndex].submenu.items[FileNewItemIndex].enabled = true;
        Menu.getApplicationMenu().items[FileMenuIndex].submenu.items[FileOpenItemIndex].enabled = true;
    }
};