const openFileService = require('../back/OpenFileService');
const mainWindow = require('../mainWindow/mainWindow');
const homeWindow = require('../home/homeWindow');
const path = require('path');
const MenuService = require('./MenuService');
const cache = require('../back/cache');

module.exports = {
    label: 'Fichier',
    submenu: [
        {
            label: 'Nouveau Projet',
            click: (menuItem, currentWindow) => {
                homeWindow.initWindow(true);
            }
        },
        {
            label: 'Ouvrir un projet',
            click: (menuItem, currentWindow) => {
                MenuService.disableNewOpenMenu();
                openFileService.open().then(pathFile => {
                    MenuService.enableNewOpenMenu();
                    if (pathFile) {
                        mainWindow.initWindow(pathFile);
                    }
                });
            }
        }
    ]
};