const cacheService = require('../back/cache');

const menu = {
    label: 'Debug',
    submenu: [
        { role: 'reload' },
        { role: 'forcereload' },
        { role: 'toggledevtools' },
        {
            label: 'clear cache',
            click: (menuItem, currentWindow) => {
                cacheService.clearCache().then(() => {
                    cacheService.loadCache();
                });
            }
        },
    ]
};
module.exports = menu;