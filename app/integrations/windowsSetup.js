const electron = require('electron');
const app = electron.app;
const FileMenu = require('../menu/FileMenu');
const DevMenu = require('../menu/DevMenu');
const ViewMenu = require('../menu/ViewMenu');
const path = require('path');
const childProcess = require('child_process');
const config = require('../config');

/*
 * Quit application when all windows are closed
 */
function configOnExit() {
    app.on('window-all-closed', function () {
        app.quit();
    });
}

/*
 * Squirrel Installer will launch electron with options to
 * allow to make installation-specific operations.
 */
function handleInstaller() {

    const squirrelCommand = process.argv[1];
    switch (squirrelCommand) {
        case '--squirrel-install':
            windowsInstall(app.quit);
            app.headless = true;
            break;
        case '--squirrel-updated':
            // Optionally do things such as:
            //
            // - Install desktop and start menu shortcuts
            // - Add your .exe to the PATH
            // - Write to the registry for things like file associations and
            //   explorer context menus
            // Always quit when done
            app.quit();
            app.headless = true;
            break;
        case '--squirrel-uninstall':
            // Undo anything you did in the --squirrel-install and
            // --squirrel-updated handlers
            windowsUninstall(app.quit);
            app.headless = true;
            break;
        case '--squirrel-obsolete':
            // This is called on the outgoing version of your app before
            // we update to the new version - it's the opposite of
            // --squirrel-updated
            app.quit();
            app.headless = true;
    }

    function windowsInstall(cb) {
        app.setAppUserModelId("com.squirrel.redigeApp.Redige");
        const target = path.basename(process.execPath);
        exeSquirrelCommand(["--createShortcut", target, '--shortcut-locations', 'StartMenu'], cb);
    }

    function windowsUninstall(cb) {
        const target = path.basename(process.execPath);
        exeSquirrelCommand(["--removeShortcut", target], cb);
    }

    function exeSquirrelCommand(args, cb) {
        const updateDotExe = path.resolve(path.dirname(process.execPath), '..', 'update.exe');
        const child = childProcess.spawn(updateDotExe, args, { detached: true });
        child.on('close', function () {
            cb();
        });
    }
}


function initMenu() {
    app.once('ready', function () {
        const menuTemplates = [FileMenu, ViewMenu];
        if (config.env === 'DEV') {
            menuTemplates.push(DevMenu);
        }
        const menu = electron.Menu.buildFromTemplate(menuTemplates);
        electron.Menu.setApplicationMenu(menu);
    });
}

module.exports.setup = function () {
    if (process.platform === 'win32') {
        configOnExit();
        handleInstaller();
        initMenu();
    }
};