'use strict';
const autoUpdater = require("electron-updater").autoUpdater;
const log = require('electron-log');

autoUpdater.addListener("update-available", function () {
    log.log('update-available');
});
autoUpdater.addListener("update-downloaded", function () {
    log.log("update-downloaded");
});
autoUpdater.addListener("error", function (err) {
    log.log("update error", err);
});
autoUpdater.addListener("checking-for-update", function () {
    log.log("checking-for-update");
});
autoUpdater.addListener("update-not-available", function () {
    log.log("update-not-available");
});

module.exports = {
    checkForUpdates: function () {
        try {
            autoUpdater.checkForUpdates();
        } catch (err) {
            //nop
        }
    }
};
