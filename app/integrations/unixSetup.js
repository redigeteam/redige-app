const electron = require('electron');
const app = electron.app;
const FileMenu = require('../menu/FileMenu');
const DevMenu = require('../menu/DevMenu');
const ViewMenu = require('../menu/ViewMenu');
const config = require('../config');

/*
 * Quit application when all windows are closed
 */
function configOnExit() {
    app.on('window-all-closed', function () {
        app.quit();
    });
}

function initMenu() {
    app.once('ready', function () {
        const menuToAdd = [FileMenu, ViewMenu];
        if (config.env === 'DEV') {
            menuToAdd.push(DevMenu);
        }
        const menu = electron.Menu.buildFromTemplate(menuToAdd);
        electron.Menu.setApplicationMenu(menu);
    });
}

module.exports.setup = function () {
    if (process.platform === 'linux') {
        configOnExit();
        initMenu();
    }
};
