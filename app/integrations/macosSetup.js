const electron = require('electron');
const app = electron.app;
const DevMenu = require('../menu/DevMenu');
const ViewMenu = require('../menu/ViewMenu');
const FileMenu = require('../menu/FileMenu');
const config = require('../config');

/*
 * Do Not Quit application when all windows are closed
 */
function configOnExit() {
    //nop : usage on mac os do not quit application on closing all windows
}

/**
 * On darwin OS, we need to add Menu to make shortcut work.
 */
function initMenu() {

    app.once('ready', function () {
        const template = [];

        template.unshift(
            {
                label: 'Rédige',
                submenu: [
                    {
                        label: 'About Rédige',
                        role: 'about'
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: 'Services',
                        role: 'services',
                        submenu: []
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: 'Hide Rédige',
                        accelerator: 'Command+H',
                        role: 'hide'
                    },
                    {
                        label: 'Hide Others',
                        accelerator: 'Command+Alt+H',
                        role: 'hideothers'
                    },
                    {
                        label: 'Show All',
                        role: 'unhide'
                    },
                    {
                        type: 'separator'
                    },
                    {
                        label: 'Quit',
                        accelerator: 'Command+Q',
                        click: function () {
                            app.quit();
                        }
                    }
                ]
            },
            FileMenu,
            {
                label: 'Edit',
                submenu: [{
                    label: 'Cut',
                    accelerator: 'CmdOrCtrl+X',
                    selector: 'cut:'
                }, {
                    label: 'Copy',
                    accelerator: 'CmdOrCtrl+C',
                    selector: 'copy:'
                }, {
                    label: 'Paste',
                    accelerator: 'CmdOrCtrl+V',
                    selector: 'paste:'
                }]
            },
            ViewMenu);

        if (config.env === 'DEV') {
            template.push(DevMenu);
        }
        const menu = electron.Menu.buildFromTemplate(template);
        electron.Menu.setApplicationMenu(menu);
    });
}


module.exports.setup = function () {
    if (process.platform === 'darwin') {
        initMenu();
        configOnExit();
    }
};