angular.module('redige-saas', ['ui.bootstrap']).config(function ($uibTooltipProvider) {
    'use strict';
    $uibTooltipProvider.options({
        'popupDelay': 500,
        'placement': "bottom",
        'appendToBody': true
    });
}).run(function ($rootScope) {
    $rootScope.userCustomCSS = window.electron.userCustomCSS;

    $rootScope.$watch(function () {
        return window.electron.templateCSS;
    }, value => {
        if (value && !$rootScope.templateCSS) {
            $rootScope.templateCSS = value;
        }
    });
});
