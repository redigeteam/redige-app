angular.module('redige-saas').factory('SearchService', function (OutlineService) {
    'use strict';
    return {
        updateMarkers: function (keyword) {
            return this.reset().then(function () {
                if (!_.isEmpty(keyword)) {
                    return getMarkers(keyword).then(function (newMarkers) {
                        return OutlineService.addMarkers(newMarkers);
                    });
                }
            });
        },
        reset: function () {
            OutlineService.removeMarkers(function (marker) {
                return marker.type === "search";
            });
            return removeAllSearchDecoratorsRecusrive(Redige.getDocument());
        }
    };

    function getMarkers(keyword) {
        let result = [];
        return getMarkersRecusrive(keyword, Redige.getDocument(), result).then(function () {
            return result;
        });
    }

    function getMarkersRecusrive(keyword, item, result) {
        if (item instanceof Redige.Section && item.content && item.content.length > 0) {
            return handleSection(keyword, item, result);
        } else if (_.isArray(item.children)) {
            let promises = [];
            item.children.forEach(function (item) {
                promises.push(getMarkersRecusrive(keyword, item, result));
            });
            return Promise.all(promises);
        }
    }

    function handleSection(keyword, section, result) {
        let maRegex = new RegExp(keyword, "gi");
        let str = section.content;
        let matchResults;
        let promises = [];
        while ((matchResults = maRegex.exec(str)) !== null) {
            promises.push(buildMarker(section, matchResults.index, matchResults.index + keyword.length).then((marker) => result.push(marker)));
        }
        return Promise.all(promises);
    }

    function buildMarker(section, start, end) {
        return Redige.patch({
            action: "addDecorator",
            sectionId: section.id,
            decorator: {end: end, start: start, type: 'search', transient: true},
        }).then(function () {
            return {
                color: "#CCA100",
                selector: "#" + section.id + " > .search",
                top: OutlineService.getTopValue(section.id),
                id: section.id,
                type: 'search',
                start: start,
                end: end
            };
        });
    }

    function removeAllSearchDecoratorsRecusrive(item) {
        let promises = [];
        if (item.decorators) {
            _.filter(item.decorators, {type: 'search'}).forEach(function (dec) {
                promises.push(Redige.patch({
                    action: "removeDecorator",
                    sectionId: item.id,
                    decoratorId: dec.id
                }));
            });
        } else if (_.isArray(item.children)) {
            item.children.forEach(function (item) {
                promises.push(removeAllSearchDecoratorsRecusrive(item));
            });
        }
        return Promise.all(promises);
    }
});