angular.module('redige-saas').directive('search', function (SearchService) {
    'use strict';
    let searchBoxInput;
    return {
        restrict: 'E',
        scope: false,
        templateUrl: './features/search/search.html',
        link: function (scope, element) {
            scope.searchActive = false;
            searchBoxInput = element[0].querySelector("#searchBoxInput");
            key('⌘+f, ctrl+f', function (event) {
                event.preventDefault();
                scope.$apply(function () {
                    scope.searchActive = true;
                    setTimeout(function () {
                        searchBoxInput.focus();
                        searchBoxInput.setSelectionRange(0, searchBoxInput.value.length)
                    }, 100);
                });
            });
            scope.onRemove = function () {
                scope.searchActive = false;
                return SearchService.reset();
            };
            scope.onKeyUp = function (event) {
                let promise;
                /*ESC*/
                if (event.keyCode === 27) {
                    promise = scope.onRemove();
                } else {
                    promise = SearchService.updateMarkers(scope.searchInput);
                }
                promise.then(function () {
                    scope.$apply();
                });
            };
        }
    };
});