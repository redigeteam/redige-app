angular.module('redige-saas').directive('redigeEditor', function () {
    'use strict';
    return {
        restrict: 'E',
        scope: false,
        templateUrl: './features/redigeEditor/editor.html',
        link: function (scope, element) {

            let redigeEditor = element[0];
            let redigeRoot = redigeEditor.querySelector("#editor");
            let toolbar = redigeEditor.querySelector("toolbar");

            redigeEditor.addEventListener('input', () => {
                if (electron) {
                    electron.setDirty();
                }
            });

            redigeEditor.addEventListener('keyup', (event) => {
                //set dirty on SUPPR
                if (event.which === 46 && electron) {
                    electron.setDirty();
                }
            });

            document.addEventListener('scrollToTitle', event => {
                const TOOLBAR_HEIGHT = 43;
                $('html, body').animate({ scrollTop: $('[level="' + event.detail.title.level + '"][numbering="' + event.detail.title.numbering + '"]').offset().top - TOOLBAR_HEIGHT }, 300);
            });

            Redige.init(redigeRoot, toolbar);
        }
    };
});