angular.module('redige-saas').factory('WarningsService', function (OutlineService) {
    'use strict';

    return {
        updateMarkers: function () {
            this.reset().then(function () {
                return getMarkers().then(function (newMarkers) {
                    return OutlineService.addMarkers(newMarkers);
                });
            });
        },
        reset: function () {
            OutlineService.removeMarkers(function (marker) {
                return marker.type === "warning";
            });
            return removeAllWarningsDecoratorsRecursive(Redige.getDocument());
        }
    };

    function getMarkers() {
        let result = [];
        return getMarkersRecursive(Redige.getDocument(), result).then(function () {
            return result;
        });
    }

    function getMarkersRecursive(item, result) {
        if (item instanceof Redige.Division && item.type === 'unknown') {
            // if it is an unknown divsion
            const message = "Division de type inconnu";
            return buildMarker(item.firstChild, item.id, result, message);
        } else if (item instanceof Redige.Section && item.numbering && (item.numbering.indexOf('.0') > -1 || item.numbering.indexOf('0.') > -1)) {
            // if it is an heading with '.0' in it
            const message = "Le niveau hiérarchique du titre n'est pas correct";
            return buildMarker(item, item.id, result, message);
        } else if (_.isArray(item.children)) {
            return Promise.each(item.children, function (item) {
                return getMarkersRecursive(item, result)
            });
        }
    }

    function buildMarker(section, topId, result, message) {
        return Redige.patch({
            action: "addDecorator",
            sectionId: section.id,
            decorator: {end: section.content.length, start: 0, type: 'warning'},
        }).then(function () {
            return {
                color: "#e74c3c",
                selector: "#" + section.id + " > .warning",
                top: OutlineService.getTopValue(topId),
                id: section.id,
                type: 'warning',
                start: 0,
                end: section.content.length,
                message: message
            };
        }).then((marker) => result.push(marker));
    }

    function removeAllWarningsDecoratorsRecursive(item) {
        let promises = [];
        if (item.decorators) {
            _.filter(item.decorators, {type: 'warning'}).forEach(function (dec) {
                promises.push(Redige.patch({
                    action: "removeDecorator",
                    sectionId: item.id,
                    decoratorId: dec.id
                }));
            });
        } else if (_.isArray(item.children)) {
            item.children.forEach(function (item) {
                promises.push(removeAllWarningsDecoratorsRecursive(item));
            });
        }
        return Promise.all(promises);
    }
});