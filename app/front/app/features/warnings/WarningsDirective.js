angular.module('redige-saas').directive('warnings', function (WarningsService, $timeout) {
    'use strict';
    return {
        restrict: 'E',
        scope: false,
        templateUrl: '',
        link: function (scope) {
            function updateMarkersCallback() {
                $timeout(() => WarningsService.updateMarkers());
            }

            document.addEventListener('paste', () => {
                updateMarkersCallback();
            });
            document.addEventListener('blockRemoved', () => {
                updateMarkersCallback();
            });
            document.addEventListener('structureChanged', () => {
                updateMarkersCallback();
            });
        }
    };

});