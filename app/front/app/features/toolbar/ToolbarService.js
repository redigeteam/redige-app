angular.module('redige-saas').factory('ToolbarService', function (ToolbarDefaultConfig) {
    'use strict';

    function fillToolbarItems(items, group) {
        if (!items[group]) {
            return _.values(ToolbarDefaultConfig[group]);
        } else if (Array.isArray(items[group])) {
            return _.compact(items[group].map(function (item) {
                if (_.isString(item)) {
                    const defaultValue = ToolbarDefaultConfig[group][item];
                    if (!defaultValue) {
                        console.error(item + ' is not a valid ' + group + ' type.');
                    }
                    return defaultValue;
                } else {
                    const defaultValue = ToolbarDefaultConfig[group][item.type] || ToolbarDefaultConfig[group]._common;
                    return _.merge(_.clone(defaultValue), item);
                }
            }));
        } else {
            console.error('fail to load toolbar ' + group + '. Invalid template. Should be an array or undefined.');
        }
    }

    return {
        parse: function (customTemplate) {
            let resultingTemplate = {};
            resultingTemplate.exports = fillToolbarItems(customTemplate, 'exports');
            resultingTemplate.inlines = fillToolbarItems(customTemplate, 'inlines');
            resultingTemplate.sections = fillToolbarItems(customTemplate, 'sections');
            resultingTemplate.divisions = fillToolbarItems(customTemplate, 'divisions');
            resultingTemplate.inserts = fillToolbarItems(customTemplate, 'inserts');
            return resultingTemplate;
        }
    };
});