'use strict';
angular.module('redige-saas').factory('ToolbarDefaultConfig', function () {

    function toggleDecoratorCommand(type) {
        let currentSelection = Redige.getDocumentSelection();
        Redige.patch({
            action: 'toggleDecorator',
            sectionId: currentSelection.startSection.id,
            decorator: { start: currentSelection.startOffset, end: currentSelection.endOffset, type: type }
        });
    }

    function linkCommand() {
        let currentSelection = Redige.getDocumentSelection();
        let existingDecorator = getActiveLinkDecorator(currentSelection.startSection, currentSelection.startOffset, currentSelection.endOffset);
        if (existingDecorator) {
            return Redige.patch({
                action: 'editLinkDecorator',
                decoratorId: existingDecorator.id,
                sectionId: currentSelection.startSection.id
            });
        } else {
            Redige.patch({
                action: 'toggleDecorator',
                sectionId: currentSelection.startSection.id,
                decorator: { start: currentSelection.startOffset, end: currentSelection.endOffset, type: 'link' }
            }).then(function (decorator) {
                if (decorator) {
                    return Redige.patch({
                        action: 'editLinkDecorator',
                        decoratorId: decorator.id,
                        sectionId: currentSelection.startSection.id
                    });
                }
            });
        }
    }

    function getActiveLinkDecorator(section, start, end) {
        let result = null;
        section.decorators.forEach(function (dec) {
            if (dec.type === 'link') {
                if ((start >= dec.start && start <= dec.end) || (end >= dec.start && end <= dec.end) || (dec.start >= start && dec.end <= end)) {
                    result = dec;
                    return dec;
                }
            }
        });
        return result;
    }

    function isSectionValidSelection(currentSelection) {
        return currentSelection && Redige.isInDocSelection() && currentSelection.startSection.parentNode === currentSelection.endSection.parentNode;
    }

    function isInlineValidSelection(currentSelection) {
        return currentSelection && Redige.isInDocSelection() && currentSelection.sections && currentSelection.sections.length === 1;
    }

    function updateSection(type, level) {
        let selection = Redige.getDocumentSelection();
        if (selection.isCollapsed) {
            let section = selection.startSection;
            let patch = { id: section.id };
            if (section.type === type && section.level === level) {
                patch.type = "hierarchical";
                patch.level = "0";
            } else {
                patch.type = type;
                patch.level = level;
            }
            Redige.patch({ action: 'updateSection', section: patch }).then(function () {
                Redige.restoreSelection();

            });
        } else if (selection.startSection.parentNode === selection.endSection.parentNode) {
            let promises = [];
            selection.sections.forEach(function (aSection) {
                let patch = { id: aSection.id, type: type, level: level };
                promises.push(Redige.patch({ action: 'updateSection', section: patch }));
            });
            Promise.all(promises).then(function () {
                Redige.restoreSelection();
            });
        }
    }

    function insertAttachment() {
        Redige.patch({
            action: 'insertAttachment',
            afterId: Redige.getDocumentSelection().startSection.id
        }).then(function (attachment) {
            return Redige.patch({
                action: 'editAttachment',
                sectionId: attachment.id
            });
        });
    }

    function insertImage() {
        Redige.patch({
            action: 'insertImage',
            data: '',
            afterId: Redige.getDocumentSelection().startSection.id
        }).then(function (image) {
            return Redige.patch({
                action: 'editImage',
                sectionId: image.id
            });
        });
    }

    function insertTable() {
        Redige.patch({
            action: 'insertTable',
            afterId: Redige.getDocumentSelection().startSection.id
        });
    }

    return {
        isEnableFunction: {
            'inlines': isInlineValidSelection,
            'sections': isSectionValidSelection,
            'divisions': isInlineValidSelection,
            'inserts': isInlineValidSelection,
        },
        exports: {
            'pdf': {
                type: 'pdf',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwCCRYTHO498wAAAUVJREFUOMud07FLW1EUBvCfr1kixAQ7ZXGQqAWnRCyIY7cMOnTVsZNzoXXo0KGLEnRwEAX/glaTodBBOtcGieJQUIqUDnaIEguGEgeXJzzz2mfqgcPHveec737ncG7fy1ev4Qjjku0n3mArepkKcRzP8OMfxcfYwSayWOkmEBafJChYxSHWEaDSTdCLbYS4jktsBknZ5Vo1EzkOoYDPWMPbXhRU8AIN7P4tIbiHYCDEIvoiPtIrwel9QwkS+n+EmXKtOvogAjzHEubLtWo6Gsj8vowtknS7rZ1OR1+fwyIeY7dcq3bwHe/P8vlv+5NP7yrIXZzfFgdYwKcwvoLpEP+g0391FVcw2Gwq1uv5cOMaH2dm33W1tB26ib0vhRhBqnMNU/iK5aTBnYw9iSs4HR52UCp96GWfW9lsjOBXK5c7/s9/cQY3vgVOm6+D7yEAAAAASUVORK5CYII=',
                tooltip: 'Exporter en PDF',
                format: 'pdf',
                process: function () {
                    electron.toPDF();
                }
            },
            'publish': {
                type: 'publish',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAk1BMVEUAAAAAff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff8Aff/XYZRKAAAAMHRSTlMAAgMEBQYIDg8YISouLzAxNztARlBRWVtrfn+Mjo+UlZqruby+wMPFzM/X5vH1+/05JPdbAAAAdElEQVQYGY3B2RaBUACG0Z9UJEmGzBxDhPje/+l0zmJxx976T2rKZUcfc6yB3ro49yTvy1kBJjhQG8kqYNeQdwaMahs4NSX5F1hIGgN55CuIpkDhqcSKleHMdMOKleFctcYatiY4e4UV33pSe1vx8jgm+u0JEXYV0r1GVPMAAAAASUVORK5CYII=',
                tooltip: 'Publier',
                format: 'publish',
                process: function () {
                    electron.publish();
                }
            },
            _common: {
                process: function () {
                }
            }
        },
        inlines: {
            code: {
                type: 'code',
                tooltip: 'Code',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwCCCcEWKLVsQAAASlJREFUOMulkz8vxEEQhp/bchMSKoViaxJRr87lTtxH0HGJUPoCwq8QToX4BkodudPIoZjQ6lQmCpVObDT+NPtLNptzLndvOdn32XdmMpXa2SujyDCiBgJIUCtBtyWoGTZBFWh6677/BUjQDQl6kpUbQEeCjkvQGwk60xMgQTeBY+A2AywDbeAdeAa6JcRk5iNgxVt3ntRngSng2lv3AzSBTgkx/cxJ/Dtv3QdAnMMacAV0jQSdA06BVg9zCWinhQhZBd6Mt+4RKIAtCbqYzWQMWIiRcx0A0yYSd4AWcClBq8mjGvDirXvKwIfAOrBkkli7wD5wkUAa+e+Jue6tuzdZbwWwF/tL11eaJyK07q17AKj8dUwSdB4QYNJb9znMLXwBRT9z3wSD6heMdnsVC2NdYgAAAABJRU5ErkJggg==',
                isEnable: isInlineValidSelection,
                process: function () {
                    toggleDecoratorCommand('code');
                }
            },
            important: {
                type: 'important',
                tooltip: 'Important',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwCCQ8biTUc2QAAAIZJREFUOMtjLK2oZKAEMOEQF2JgYPjKwMDwH4r3kWoAGwMDAxcSX5BUA74R4BM04DsBPkEDfjMwMPyhxAB0Td8oNYAsF9xDYt/HpYgFjwGWxCQknAaEqS4VYGBgEIFyH666Hf2bVC8cZGBguA3FLeSEATcONtEGPMPBJi4MVt2OtqMkNxINAKAIHgRnEi3JAAAAAElFTkSuQmCC',
                isEnable: isInlineValidSelection,
                process: function () {
                    toggleDecoratorCommand('bold');
                }
            },
            quote: {
                type: 'quote',
                tooltip: 'Citation',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwCCC0SVpmIagAAAMxJREFUOMutk7EKwkAMhr+WG3wAlS66iXPfwEXovYGP4SLo0MFJHN2cBKe+gNDZza2zCC4uXaWjIC4pXKHXG84sIfnCn1zIBav1Bh9TRZRPgSMwMvIVkMalvri4ArbArEX8BAxdPAT6lukG4jt5iKf9RaBy1FQugT3w6qjp5Cou9Q0Y14kiyhdAVscu3raDh/iPpWmDtwkk4u8WgQZXMlYPmABzIJWCszG2lSsJdsDS6HIFDkZs5fUT3sAXeMrpJnGpzR1YeeD7G70v8QdkTz17dBUUGgAAAABJRU5ErkJggg==',
                isEnable: isInlineValidSelection,
                process: function () {
                    toggleDecoratorCommand('italic');
                }
            },
            link: {
                type: 'link',
                tooltip: 'Lien',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwCCDQwGPlglgAAAT9JREFUOMuN000o5lEUx/GPl5qwUKwsLKysNClhwbBRUhRppKGsyMpKhpWmNGq2ysJqpmnkrcjCwsv2MfSUjQVKFl6yQVO2sjnqn8Hf2dx7z7n3e7u/87s5+2MfRfRhCPeYxK53RH6MY5hO5JvRjs00QC6+xOFb1KAnwKtofA/gGx7QhiwWMYxCLKAoDVCKOxwk8gtYRhmq0wDrKIlD+ejCBapiz3UaYASH6MAvnKMAlVjDCSpQH/n/ADdowSl68Rd5UW/FUtQyOELdcwBcoQG/sYdxDOADunGMFZRjC03PffAE6U+svybmZXHzNmawES3O5r6hT2fiGVP4h9lwbEG031uAkxi78CO8IgSH4jTAaIg3iPnQoxY7UZ9LA1zFOw/xOfyQCeNN4GcaAC7xCX+Qg7MQ+vtLXXgtbuLDvRiPnp1CfgiqAAcAAAAASUVORK5CYII=',
                isEnable: isInlineValidSelection,
                process: linkCommand
            },
            _common: {
                isEnable: isInlineValidSelection,
            }
        },
        sections: {
            normal: {
                type: 'normal',
                tooltip: 'Normal',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwCCAEh0HiC0gAAAG1JREFUOMut0jEOwkAMRNG31hZQkQql5o5IcFnaKB2bJnRQ5QI2X3Lp0Xxp2v3xVKFjxpT8f3e8cE4G7IGtYLAdCmkCK77JWwOtUKB1XKsKS0FhicIGYOq44ZIMGKHIscRT8v8TGIUC4y9LLPEDSKErPE5Z/6AAAAAASUVORK5CYII=',
                isEnable: isSectionValidSelection,
                process: function () {
                    updateSection('hierarchical', "0");
                }
            },
            header: {
                type: 'header',
                tooltip: 'Titre',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwCCAUSC8QmwAAAAKtJREFUOMu90T2KAlEQxPHfe0xgpiDIxsLeQQw29Qh6CUPBvYaB7Ak8gCcYEMTUCxgJfsALJjPSTSbYUN/IFjQdVfOv6gDbbjdgEtkMUjp6QbHeP1jd+fSiinov8YUHlGU5xeJpgmFKe4R64P4sQag7WGOEM8bDlHb+S2E2/76gl+m/xj+5swDeEuGEj0z/OaLTAKBToI925oEqNu2gwAGtTP8tomoAUDV+4y91KyIVAc815gAAAABJRU5ErkJggg==',
                isEnable: isSectionValidSelection,
                process: function () {
                    updateSection('hierarchical', "1");
                }
            },
            olist: {
                type: 'olist',
                tooltip: 'Liste Ordonnée',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwCBzQoAMm//QAAAWxJREFUOMu9k79LQlEcxc+99/kstcUcbRCKspamECQLIWpp6BcYRDW4RUSQmLQ2RVFhRVOESVGBYRC0hUhISENCUUNLSA2FRJjJ+3Fvi3/Aew2d5cB3+MA5nC8BgLg/RTjlIdVeyS5cThZhQrTmu5TTQ+nH2gKTkmq+AyBABBEAEFmMOQC4DQNmr4fv4v4UEUSQ2n0KwJbhCJuB03MAHqpJ+2t9SR8AFf8lAgAbPScJpkkjnOpnc9mxib8ARqnOikSQXMVVavjwPnfVijVWomD6AzSW4kw/iKbD5Yg31g6g1TCAqpYLQXmSM/1zPXjkeUXhCUDGUITVgYRTLtseIYgGALqsDs1fhW5MdbDan+i0VOoVwThlquX+pTtvA9BkOIJctudARIFoUkfFVXICmDY1JMXx7RMEjVpdNRlNhxUzQyIAsDK418YUS1Au27e/3G+9S8czGcPPtDwep9K7NSVVrc2arOQF5bdmhvQLOyZ/VHzBH0kAAAAASUVORK5CYII=',
                isEnable: isSectionValidSelection,
                process: function () {
                    updateSection('olist', "0");
                }
            },
            ulist: {
                type: 'ulist',
                tooltip: 'Liste à puces',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwCBzYkO0mRVAAAAKVJREFUOMvd0qESAXEUxeGPXUmhGlnwCIpRjaJ4Ad4Ab6CpCq8g8AJW+1cbeA8qRdmwcZdgxkn33Jk7c86dH1+qclj2eljgglWI+nW0C94/Y+zRwhjXbN4UTVDFPefveJVo8IoxxBTpeB3OhDN2ZRJ8/cQbupkfhKjfwbZohSoauUUTtRIBajEmmCPFEXWciibwHyA9cv7xCUgjzDKQEkJSBqTf6w1u0i2gpaAC/gAAAABJRU5ErkJggg==',
                isEnable: isSectionValidSelection,
                process: function () {
                    updateSection('ulist', "0");
                }
            },
            _common: {
                type: 'unknown',
                isEnable: isSectionValidSelection,
                process: function () {
                }
            }
        },

        divisions: {
            blockcode: {
                type: 'blockcode',
                tooltip: 'Bloc de code',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwBFA8vrsp3oQAAAR1JREFUOMutkb1KA1EQhb+5ezHaaCAgGlDBQlKltQuKm8IXsNt0YmUZSApB0iSwnU+gbvARbIxgkUZBSB5BiBu1ELWIAfVeC1cQC/OzHhimmHNmzmEEYPPgdOItmd1C1G7DS68yArRbDyvvsC1W2saYys9hsVTODNA/aWsQUYBYK8raSGiHNPCqzgvpPf3YWrLWnAhqn9HwQlxIsVS+B2bH1D8oQOIY+JcIXWBuTP2dApIxDCQ1sAzMjLngWQPhH4QU0G9ndhaMkzhrePOLvwlqwIW+X6v2PhydQ0wTwA3CSzfoeGuHFwkA5deqAkxHfTKqFDDl16q9r185OTE0AbDSEnGOtbNyPXTQfL17sx50sgD5ILzaOLotfDuIjU9Gf1R4SNQQAgAAAABJRU5ErkJggg==',
                isEnable: isSectionValidSelection,
                process: function (currentSelection) {
                    Redige.patch({
                        action: 'wrapInDivision',
                        startSectionId: currentSelection.startSection.id,
                        endSectionId: currentSelection.endSection.id,
                        divisionType: 'blockcode'
                    });
                }
            },
            blockquote: {
                type: 'blockquote',
                tooltip: 'Bloc de citation',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QAdQB4AHm7SiSwAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwBFBA7eUqtQgAAAKZJREFUOMutk70KwjAUhb9cMggZ7CDiqlLw4boW2idQcO27ORQc/emkQsGtLgk4KCm5/eDCGXJyTy65ALgmH/B86zEY1+RDX7QmmIMGKKt6F/E/JJLgFKmzoOOFFlNW9R1YJvo7AYwmwCRPuAKrRP9NgEwRILPABpgnXvC0wCV26njY/x2WBd7AbEy78Ev7ojVBC7DwQ7SA893WvhywZUJEs7k/l+4DyRY0pESfCWIAAAAASUVORK5CYII=',
                isEnable: isSectionValidSelection,
                process: function (currentSelection) {
                    Redige.patch({
                        action: 'wrapInDivision',
                        startSectionId: currentSelection.startSection.id,
                        endSectionId: currentSelection.endSection.id,
                        divisionType: 'blockquote'
                    });
                }
            }
        },
        inserts: {
            image: {
                type: 'image',
                tooltip: 'Image',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QAdQB4AHm7SiSwAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwBFBMbaQneSQAAAO5JREFUOMul0z9Lw1AUBfBfYsXiLkh3QcRP4JpJR0Hcuuji1qWk/QgFwbq6dHPJJLi6ujrUSQRBKIiToyg0Li8Qg0iTHrhwHtw/5953b9QfDC2D2JKIcYu8oU1jHGAHK9jDJqIFbA27cXh84zpNsvs0yd6wtYD6r18zSJPsuMSfKs5dnP47xNHd0XmJd0o+J5jgCmfVBFF/MMyD5Gds4CO0VGAe2izQw2XgefUb9/GJd1yESUcVnzEe/9qDbpAaByW9uos0abJYrRJ/qRH3Wk6Qo43tmsXXiwQ3mDY8hYcWDtHBas3gOWZFC7Om1/gDR949EaGcGfwAAAAASUVORK5CYII=',
                isEnable: isSectionValidSelection,
                process: insertImage
            },
            attachment: {
                type: 'attachment',
                tooltip: 'Pièce Jointe',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAABIAAAASABGyWs+AAAACXZwQWcAAAAQAAAAEABcxq3DAAAA/UlEQVQ4y+3RsS6kARTF8d/9ZkS9jbCRrAYNiYlaLdFrdARjaoWCQuIBJETCdgpReAPZLRUSCokHmEgUCgmSbTaz392GZo316Z3y5tx/zj03vKFcMiIM6XEZe+7f8hVdl5u2FH4Kqzqus2WqMiCbhjEvjcWBaaUFpZ3qCUIDZ/HdI3hwivHqgFJd6OSyuVxxFCf+oPacLit18BF9Av4PeJS+ZcsoflUHFDpSXa8fwpPSubSRs2q8fmO9C7SNhl2/g5mXYS6ZkG7eT9DvAnea1pOAbPmisCNs/2uPbnflokE1x+hDW2hIhwasxabyXQDkpsKtSYWvwlXsa3fz/QVUaEZaE9VdzgAAAABJRU5ErkJggg==',
                isEnable: isSectionValidSelection,
                process: insertAttachment
            },
            table: {
                type: 'table',
                tooltip: 'Tableau',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAeFBMVEX///92enp0eXl1eHl1eHl1eXl1eHp1d3l1eHl0eHp2dntwgIBzeXl0eXl4eHh2dnZ2eHl1eHh1eHl0eHh1eHp1eHp2eHp2eXl1d3p2dnx3d3d0eHh1eXl1eXl3d3dyeXl0d3p1eHh1d3l0eHp1eXl2eHh1eHkAAAAJwyrXAAAAJnRSTlMAQ7jI4MmwvMTDOBAoOREp5tjkimBxgF9eJzxRPUwPJkudeIiUd/WGv5AAAAABYktHRCctD6gjAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAB3RJTUUH4AcNECcURuNwhwAAAIVJREFUGNNtz0kSgjAARNEGYlDmMIUAhjF9/yOapSJv+Rdd1cBVEEZfwgCCPwT4kDLmU8oXYykjggmQMgMypkDiQw4ULIGSBZD7UCklWCtVUyhV+dC0bcde656d1s3dxmDMyMmYibMxgw8X4NvahbO1Ixdr15sNse37wdO5k4dzm/g/d/UB/1sR099nfo0AAAAldEVYdGRhdGU6Y3JlYXRlADIwMTYtMDctMTNUMTY6Mzk6MjArMDI6MDBc6U0jAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE2LTA3LTEzVDE2OjM5OjIwKzAyOjAwLbT1nwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAASUVORK5CYII=',
                isEnable: isSectionValidSelection,
                process: insertTable
            }
        }
    };
});