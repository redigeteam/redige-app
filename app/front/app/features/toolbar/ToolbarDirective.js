angular.module('redige-saas').directive('toolbar', function (ToolbarService, $timeout) {
    'use strict';
    return {
        restrict: 'E',
        scope: false,
        templateUrl: './features/toolbar/toolbar.html',
        link: function (scope) {
            scope.toolbar;
            scope.save = electron.save;

            key('⌘+s, ctrl+s', function (event) {
                event.preventDefault();
                electron.save();
            });

            scope.$watch(
                function () {
                    try {
                        return Redige.getDocument().template.toolbar;
                    } catch (e) {
                        return null;
                    }
                }, function (newTemplate) {
                    if (newTemplate && !scope.toolbar) {
                        scope.toolbar = ToolbarService.parse(newTemplate);
                    }
                }
            );

            scope.Redige = Redige;

            document.addEventListener('selectionChange', function () {
                if (scope.toolbar) {
                    scope.currentSelection = Redige.getDocumentSelection();
                    $timeout(() => {
                        updateToolbarDisableStatus(scope.toolbar.actions, scope.currentSelection);
                        updateToolbarDisableStatus(scope.toolbar.inlines, scope.currentSelection);
                        updateToolbarDisableStatus(scope.toolbar.sections, scope.currentSelection);
                        updateToolbarDisableStatus(scope.toolbar.divisions, scope.currentSelection);
                        updateToolbarDisableStatus(scope.toolbar.inserts, scope.currentSelection);
                    });
                }
            });
        }
    };

    function updateToolbarDisableStatus(btns, currentSelection) {
        if (btns) {
            btns.forEach(function (btn) {
                if (btn.isEnable) {
                    btn.isDisabled = !btn.isEnable(currentSelection);
                } else {
                    btn.isDisabled = false;
                }
            });
        }
    }

});