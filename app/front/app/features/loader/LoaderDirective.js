angular.module('redige-saas').directive('loader', function () {
    'use strict';
    return {
        restrict: 'E',
        scope: false,
        templateUrl: './features/loader/loader.html',
        link: function (scope) {
            scope.isLoaderActive = false;

            document.addEventListener('setLoader', event => {
                scope.isLoaderActive = event.detail.isActive;
                scope.$apply();
            });
        }
    };
});
