angular.module('redige-saas').directive('outline', function (OutlineService) {
    'use strict';
    return {
        restrict: 'E',
        scope: false,
        templateUrl: './features/outline/outline.html',
        link: function (scope) {
            scope.markers = OutlineService.markers;
            scope.outlineScroll = outlineScroll;
        }
    };
    function outlineScroll(marker) {
        let node = $(marker.selector || "#" + marker.id);
        let currentColor = node.css("backgroundColor");
        let blinkColor = 'transparent';
        let duration = 100;
        const TOOLBAR_HEIGHT = 43;

        scroll(function () {
            iterate(blinkColor, function () {
                iterate(currentColor, function () {
                    iterate(blinkColor, function () {
                        iterate(currentColor);
                    });
                });
            });
        });

        function scroll(cb) {
            $('html, body').animate({scrollTop: node.offset().top - TOOLBAR_HEIGHT}, 300, cb);
        }

        function iterate(color, cb) {
            node.animate({backgroundColor: color}, duration, cb);
        }
    }

});
