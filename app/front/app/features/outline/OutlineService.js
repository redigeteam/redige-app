angular.module('redige-saas').factory('OutlineService', function () {
    'use strict';
    /*{
     color: "#CCA100",
     top: "250px",
     type: 'search',
     selector: "string",
     id: "id",
     from:12
     to:1
     }*/
    let markers = [];

    return {
        markers: markers,

        removeMarkers: function (iteratorFn) {
            _.remove(markers, iteratorFn);
        },
        addMarkers: function (newMarkers) {
            newMarkers.forEach((e) => markers.push(e));
        },
        getTopValue: function (elementId) {
            return (Math.round(document.getElementById(elementId).offsetTop * 10000 / $(document).height()) / 100) + "%";
        }
    };
});