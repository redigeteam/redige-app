'use strict';

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Empties folders to start fresh
        clean: {
            dev: {
                src: 'app/index.html'
            }
        },

        // Automatically inject Bower components into the app
        wiredep: {
            dev: {
                src: ['app/index.html'],
                "overrides": {
                    "bootstrap": {
                        "main": ["dist/js/bootstrap.js", "dist/css/bootstrap.css"],
                    },
                    "jquery-color": {
                        "main": "./jquery.color.js"
                    }
                }
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dev: {
                src: 'index.template.html',
                dest: 'app/index.html'
            },
            styles: {
                expand: true,
                cwd: 'app/assets/styles',
                dest: '.tmp/styles/',
                src: '{,*/}*.css'
            }
        },

        fileblocks: {
            dev: {
                files: [
                    {
                        src: 'app/index.html',
                        blocks: {
                            'app': {
                                src: ['app/app.js', 'app/**/*.js', '!app/bower_components/**/*.js'],
                                rename: function (dest, src) {
                                    //remove the first part of the path (app/)
                                    return src.substring(4);
                                }
                            },
                            'css': {
                                src: ['app/**/*.css', '!app/bower_components/**/*.css'],
                                rename: function (dest, src) {
                                    //remove the first part of the path (app/)
                                    return src.substring(4);
                                }
                            }
                        }
                    }
                ]
            }
        }
    });


    grunt.registerTask('default', 'dev');

    grunt.registerTask('dev', [
        'clean:dev',
        'copy:dev',
        'wiredep:dev',
        'fileblocks:dev'
    ]);

};
