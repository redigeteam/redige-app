const _ = require('lodash');
const spellingService = require('redige-spelling');
const Promise = require('bluebird');
const config = require('../config');

function checkDocument(document) {
    const promises = [];
    document.children.forEach(section => {
        if (shouldCheck(section)) {
            if (section.content) {
                promises.push(checkSection(section));
            }
            if (section.children) {
                promises.push(Promise.map(section.children, subSection => checkSection(subSection)));
            }
        }
    });
    return Promise.all(promises);
}

function shouldCheck(section) {
    return config.enableSpelling && (!section.classList.contains('code') && !section.parent.classList.contains('blockcode') && !_.isEmpty(section.content));
}

let lastCallContent;

function checkSection(section) {
    if (lastCallContent === section.content) {
        return Promise.resolve();
    } else {
        lastCallContent = section.content;
    }

    let promise = Promise.resolve();

    if (section.decorators !== undefined) {
        const spellingDecorators = section.decorators.filter(decorator => {
            return decorator.type === 'spelling-error' || decorator.type === 'grammar-error';
        });

        if (spellingDecorators.length > 0) {
            promise = Redige.patch({
                action: 'removeDecorators',
                sectionId: section.id,
                decoratorIds: spellingDecorators.map(d => d.id)
            });
        }
    }

    return promise.then(() => {
        console.log('check ' + section.decorators);
        return spellingService.check(section.content);
    }).then(warnings => {
        return warnings.map(warning => {
            return {
                'start': warning.from,
                'end': warning.to,
                'transient': true,
                'type': warning.id === 'HUNSPELL_RULE' ? 'spelling-error' : 'grammar-error',
                'suggestions': warning.replacements,
                'message': warning.description
            };
        });
    }).then((decoratorsToAdd) => {
        if (!_.isEmpty(decoratorsToAdd)) {
            return Redige.patch({
                action: 'addDecorators',
                sectionId: section.id,
                decorators: decoratorsToAdd
            })
        }
    });
}

module.exports = {
    checkDocument: checkDocument,
    checkSection: checkSection,
    shouldCheck: shouldCheck
};
