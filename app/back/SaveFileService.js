const _ = require('lodash');
const log = require('electron-log');
const fs = require('bluebird').promisifyAll(require('fs-extra'));
const path = require('path');
const config = require('./../config');
const cache = require('./cache');
const markdownExporter = require('./exporter/MarkdownExporter');
const archiver = require('archiver');
const sanitize = require('sanitize-filename');

function save(document, callback) {

    const filePath = document.file.path;

    if (filePath) {
        const fileInfo = document.file || {};
        const parsedPath = path.parse(filePath);
        document.file = {
            path: filePath,
            base: parsedPath.base,
            parentPath: fileInfo.parentPath,
            workingDirectory: parsedPath.dir
        };

        let docContent;
        if (filePath.endsWith('.md')) {
            docContent = markdownExporter.convert(document);
        } else if (filePath.endsWith('.json')) {
            const cleanedDoc = _.omit(document, ['template', 'file', 'storage', 'allTitles']);
            docContent = JSON.stringify(cleanedDoc);
        } else if (filePath.endsWith('.rdg')) {
            docContent = JSON.stringify({
                template: { name: document.template.name },
                storage: { parser: document.storage.parser }
            });
        }

        cacheLastEditedFile(fileInfo);
        if (isNewBundleDocument(filePath, fileInfo.workingDirectory)) {
            const jsonContent = JSON.stringify(_.omit(document, ['template', 'file', 'storage', 'allTitles']));
            const tempChildrenFile = path.join(config.redigeTmp, 'data.json');
            const tempRdgFile = path.join(config.redigeTmp, 'project.rdg');

            const jsonDataPromise = fs.writeFileAsync(tempChildrenFile, jsonContent);
            const rdgPromise = fs.writeFileAsync(tempRdgFile, docContent);
            return Promise.all([jsonDataPromise, rdgPromise]).then(() => {
                saveNewBundle(filePath, tempRdgFile, tempChildrenFile);
                callback(null, document.file);
            });
        } else {
            return fs.writeFileAsync(filePath, docContent).then(() => {
                if (isBundleDocument(fileInfo.workingDirectory)) {
                    saveBundle(fileInfo);
                }
                callback(null, document.file);
            });
        }
    }
}

function createNewProject(newProject) {
    newProject.name = sanitize(newProject.name);

    const projectFolder = path.join(newProject.path, newProject.name);
    return fs.mkdirAsync(projectFolder).then(() => {
        const rdgContent = JSON.stringify({
            template: { name: newProject.template },
            storage: { parser: 'json' }
        });
        return createProjectFiles(rdgContent, projectFolder).then(filePaths => {
            let fileToOpen = filePaths[0];
            if (newProject.format === 'bundled') {
                fileToOpen = path.join(newProject.path, newProject.name) + '.rdg';
                return saveNewBundle(fileToOpen, filePaths[0], filePaths[1]).then(() => {
                    return fs.removeAsync(projectFolder);
                }).then(() => fileToOpen);
            }
            return fileToOpen;
        });
    }).catch(e => {
        log.error(e);
    });
}

function createProjectFiles(rdgContent, projectFolder) {
    const rdgPath = path.join(projectFolder, 'project.rdg');
    const dataPath = path.join(projectFolder, 'data.json');
    return Promise.all([fs.writeFileAsync(rdgPath, rdgContent), fs.writeFileAsync(dataPath, JSON.stringify({}))]).then(() => {
        return [rdgPath, dataPath];
    });
}


function cacheLastEditedFile(fileInfo) {
    const parentPath = fileInfo.parentPath;
    const workingDirectory = fileInfo.workingDirectory;

    // place current file in cache
    let pathToCache;
    if (workingDirectory !== undefined || parentPath !== undefined) {
        if (isBundleDocument(workingDirectory)) {
            pathToCache = fileInfo.path.replace(workingDirectory, '');
        } else if (parentPath !== undefined) {
            pathToCache = fileInfo.path.replace(path.parse(parentPath).dir, '');
        }

        const lastOpenendFiles = cache.get('lastOpenedFiles') || {};
        lastOpenendFiles[parentPath] = { path: pathToCache };

        if (fileInfo.path.endsWith('.json')) {
            lastOpenendFiles[parentPath].selection = fileInfo.selection;
        }

        cache.put('lastOpenedFiles', lastOpenendFiles);
    }
}

function saveBundle(fileInfo) {
    const archive = initArchive(fileInfo.parentPath);
    archive.directory(fileInfo.workingDirectory, '');
    archive.finalize();
}

function saveNewBundle(filePath, rdgFile, dataFile) {
    const archive = initArchive(filePath);
    archive.file(rdgFile, { name: 'project.rdg' });
    archive.file(dataFile, { name: 'data.json' });
    return archive.finalize();
}

function initArchive(filePath) {
    const output = fs.createWriteStream(filePath);
    const archive = archiver('zip');

    output.on('close', function () {
        log.info('Bundled document saved at', filePath);
    });
    archive.on('error', function (err) {
        throw err;
    });
    archive.pipe(output);

    return archive;
}

function isBundleDocument(workingDirectory) {
    return workingDirectory !== undefined && workingDirectory.startsWith(config.workspaceDir);
}

function isNewBundleDocument(filePath, workingDirectory) {
    return filePath.endsWith('.rdg') && workingDirectory === undefined;
}

module.exports = {
    save: save,
    createNewProject: createNewProject
};
