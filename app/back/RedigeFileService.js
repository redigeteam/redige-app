const electron = require('electron');
const { BrowserWindow, dialog, shell } = electron;
const log = require('electron-log');
const fs = require('bluebird').promisifyAll(require('fs'));
const os = require('os');
const path = require('path');
const url = require('url');
const config = require('./../config');
const cache = require('./cache');
const redigeAPI = require('./RedigeAPIService');
const markdownExporter = require('./exporter/MarkdownExporter');
const markdownImporter = require('./importer/MarkdownImporter');
const RevealExporter = require('./exporter/RevealJsExporter');
const HTMLExporter = require('./exporter/HTMLExporter');
const PdfExporter = require('./exporter/PdfExporter');
const EpubExporter = require('./exporter/EpubExporter');
const fsExtra = require('fs-extra');
const _ = require('lodash');
const klaw = require('klaw');

const ensureRedigeHome = function () {
    fsExtra.ensureDirSync(config.redigeHomePath);
    fsExtra.ensureDirSync(config.templateFolder);
    fsExtra.ensureFileSync(config.customCSSPath);
    fsExtra.ensureDirSync(config.defaultTemplate);
    fsExtra.ensureDirSync(config.workspaceDir);
    fsExtra.copySync(path.join(__dirname, 'template/default'), config.defaultTemplate);
};

function exportPDF(children, fileDirectory, workingDir, templateName) {
    if (workingDir) {
        const pdfPath = path.join(workingDir, path.parse(workingDir).name + '.pdf');

        return PdfExporter.exportToPDF(pdfPath, children, fileDirectory, templateName).then(function () {
            shell.openExternal("file://" + pdfPath);
        }).catch(function (err) {
            log.error(err);
            dialog.showErrorBox("Ooups", err.toString());
        });
    } else {
        dialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
            type: "error",
            message: "Sauver avant d'exporter. Avant de pouvoir exporter ce document, il faut l'enregistrer.",
            buttons: ["OK"]
        });
        return Promise.reject();
    }
}

function publish(document) {
    return redigeAPI.publish(document);
}

function exportReveal(document, revealFolder) {
    return createRevealFile(document, revealFolder).then((revealPath) => {
        openBrowser(revealPath);
    });
}

function createRevealFile(document, revealFolder, asPDF) {
    if (document.file.workingDirectory) {
        const revealPath = path.join(config.templateFolder, document.template.name, revealFolder, path.parse(document.file.workingDirectory).name) + '.html';
        return RevealExporter.toRevealFormat(revealPath, document, asPDF).then(() => {
            return revealPath;
        }).catch(err => {
            log.error(err);
            dialog.showErrorBox("Ooups", err.toString());
        });
    } else {
        dialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
            type: "error",
            message: "Sauver avant d'exporter. Avant de pouvoir exporter ce document, il faut l'enregistrer.",
            buttons: ["OK"]
        });
        return Promise.reject("Sauver avant d'exporter.");
    }
}

function openBrowser(url) {
    const platform = os.platform();
    if (platform === 'win32') {
        shell.openExternal(url);
    } else if (platform === 'darwin') {
        shell.openExternal("file://"+url);
    } else {
        require('child_process').exec('xdg-open ' + url);
    }
}

function exportRevealPdf(document, revealFolder) {
    return createRevealFile(document, revealFolder, true).then(revealPath => {
        const loadUrl = revealPath + '?print-pdf';
        const options = {
            marginsType: 0,
            printBackground: true,
            printSelectionOnly: false,
            landscape: true
        };
        const pdfFilePath = document.file.path.slice(0, -4) + '-reveal.pdf';
        setTimeout(() => {
            return toPDF(loadUrl, options, pdfFilePath);
        }, 1000)

    })
}

function exportEpub(document) {
    return EpubExporter.toEpub(document).then(epubPath => {
        dialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
            type: "info",
            message: "ePUB généré ! \nEmplacement du fichier : \n" + epubPath,
            buttons: ["OK"]
        });
    }).catch(err => {
        log.error(err);
        dialog.showErrorBox("Ooups", err.toString());
    });
}

function copyFile(filePath, docFolder) {
    if (docFolder) {
        const newFilename = path.join(docFolder, path.parse(filePath).base);
        const readWrite = function () {
            fs.createReadStream(filePath).pipe(fs.createWriteStream(newFilename))
        };

        return fs.accessAsync(newFilename, fs.constants.R_OK).then(() => {
            dialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
                type: "error",
                message: "Un fichier portant le même nom existe dans le répertoire de travail. \n Remplacer ce fichier ?",
                buttons: ["Oui", "Non"]
            }, function (btnIndex) {
                if (btnIndex === 0) {
                    readWrite();
                }
            });
        }).catch(() => {
            readWrite();
        });
    } else {
        dialog.showErrorBox('Copie impossible', 'Veuillez sauver le document pour insérer l\'image dans votre dossier');
        // supprimer le bloc inaccessible
    }

}

function toPDF(loadUrl, options, outputFile) {
    const win = new BrowserWindow({ show: false });
    win.loadURL('file://' + loadUrl, { "extraHeaders": "pragma: no-cache\n" });
    win.webContents.on('did-finish-load', () => {
        win.webContents.printToPDF(options, (error, data) => {
            if (error) log.error(error);
            return fs.writeFileAsync(outputFile, data).then(() => {
                shell.openExternal('file://' + outputFile);
                win.destroy();
            }).catch(err => {
                log.error(err);
                dialog.showErrorBox("Ooups", err.toString());
            })
        })
    });
}

module.exports = {
    init: function () {

        /*
         * use a persistent cache
         */
        cache.loadCache();

        /*
         * ensure having a .redige folder in home.
         */
        ensureRedigeHome();

    },
    exportPDF: exportPDF,
    publish: publish,
    exportReveal: exportReveal,
    exportRevealPdf: exportRevealPdf,
    exportEpub: exportEpub,
    copyFile: copyFile,
    openBrowser: openBrowser
};
