const electron = require('electron');
const cache = require('./cache');
const config = require('./../config');
const { BrowserWindow, dialog } = electron;
const fs = require('bluebird').promisifyAll(require('fs'));
const Promise = require('bluebird');
const klaw = require('klaw');
const log = require('electron-log');
const markdownImporter = require('./importer/MarkdownImporter');
const path = require('path');
const templateService = require('./TemplateService');
const decompress = require('decompress');
const _ = require('lodash');

const loadingWindow = require('../loadingWindow/loadingWindow');

// field tagged with * are field not saved but defined when openned
/*
{
    template: {
        name:"the name of the template"
        *path:"the path of the template"
        *cssPath:"the css path of the template
    },
    storage: {
        parser : "md | json"
        *type: "exploded | bundle"
        *originalPath : "- rdg file (the project.rdg file for exploded)"
                        "- the bundle .rdg file for bundle"
                        "- null if never saved file"
        *files : "the list of the exploded files"
        *workingDirectory "the parent of rdg file for exploded, the dezipped directory for bundle"
    }
}
*/

function open() {
    let chosenFile = dialog.showOpenDialog(BrowserWindow.getFocusedWindow(), {
        title: "Open File",
        defaultPath: cache.get('defaultPath'),
        filters: [
            { name: 'Redige File', extensions: ['rdg'] }
        ],
        properties: ['openFile']
    });
    if (chosenFile && chosenFile.length > 0) {
        const currentDir = path.parse(chosenFile[0]).dir;
        cache.put('defaultPath', currentDir);
        addToRecents(chosenFile[0]);
        return Promise.resolve(chosenFile[0]);
    } else {
        return Promise.resolve(null);
    }
}

function addToRecents(path) {
    const newRecents = cache.get('recents') || [];
    newRecents.push(path);
    cache.put('recents', _.uniq(newRecents));
}

function openRdgCallback(rdgPath) {
    return openFromRdg(rdgPath).catch(e => {
        log.error(e);
        dialog.showErrorBox('Document non ouvert', 'Erreur dans la lecture du fichier !');
        return Promise.resolve(null);
    });
}

function openFromRdg(rdgFilePath) {
    return isRedigeMetaJsonFile(rdgFilePath).then(isJson => {
        if (isJson)
            return openExplodedFile(rdgFilePath);
        else
            return openBundledFile(rdgFilePath);

    }).then(metaRDFFile => {
        return resolveTemplateName(metaRDFFile);
    }).then(metaRDFFile => {
        return addTemplateCss(metaRDFFile);
    }).then(metaRDFFile => {
        return metaRDFFile;
    });
}

function openNoDialog(pathObject, template) {
    if (pathObject.filePath.endsWith('.md'))
        return openMd(pathObject, template);
    else if (pathObject.filePath.endsWith('.json'))
        return openJson(pathObject, template);
    else throw new Error('unknown format');
}

function openMd(pathObject, template) {
    return fs.readFileAsync(pathObject.filePath, 'utf-8').then(data => {
        const mdImport = {};
        mdImport.children = markdownImporter.parse(data).content.children;
        mdImport.template = template;
        mdImport.storage = {};
        mdImport.storage.type = pathObject.storage.type;
        mdImport.storage.files = [];
        mdImport.children = mdImport.children || [];
        mdImport.file = {
            path: pathObject.filePath,
            base: path.parse(pathObject.filePath).base,
            directory: path.parse(pathObject.filePath).directory,
            parentPath: pathObject.parentPath,
            workingDirectory: pathObject.storage.workingDirectory
        };
        return mdImport;
    });
}

function openJson(pathObject, template) {
    return fs.readFileAsync(pathObject.filePath, 'utf8').then(content => {
        const jsonImport = JSON.parse(content);
        jsonImport.template = template;
        jsonImport.storage = {};
        jsonImport.storage.type = pathObject.storage.type;
        jsonImport.storage.files = [];
        jsonImport.children = jsonImport.children || [{}];
        jsonImport.file = {
            path: pathObject.filePath,
            base: path.parse(pathObject.filePath).base,
            directory: path.parse(pathObject.filePath).directory,
            parentPath: pathObject.parentPath,
            workingDirectory: pathObject.storage.workingDirectory
        };
        return jsonImport;
    });
}

function isRedigeMetaJsonFile(path) {
    return new Promise((resolve, reject) => {
        let result = false;
        const reader = fs.createReadStream(path, { start: 0, end: 64 });
        reader.setEncoding("utf-8");
        reader.on('data', chunk => {
            result = chunk.indexOf("{") === 0;
        });
        reader.on('end', () => {
            resolve(result);
        });
        reader.on('error', err => {
            reject(err);
        });
    });
}

function openExplodedFile(filePath) {
    return fs.readFileAsync(filePath, 'utf8').then(content => {
        const rdfFile = JSON.parse(content);
        const dir = path.dirname(filePath);

        rdfFile.file = {
            path: filePath,
            base: path.parse(filePath).base,
            directory: path.parse(filePath).directory
        };
        rdfFile.storage = rdfFile.storage || {};
        rdfFile.storage.originalPath = filePath;
        rdfFile.storage.workingDirectory = dir;
        rdfFile.storage.type = "exploded";
        rdfFile.storage.files = [];
        const extension = "." + rdfFile.storage.parser;

        return filesWithExtension(dir, extension).then(filesAndFolders => {
            rdfFile.storage.files = filesAndFolders;

            // get titles from all workspace
            const files = filesAndFolders.filter(file => file.path.endsWith(extension));
            return getTitlesFromFiles(files).then(allTitles => {
                rdfFile.allTitles = allTitles;
                return rdfFile;
            });
        });
    });
}

function getTitlesFromFiles(files) {
    return Promise.map(files, file => {
        return fs.readFileAsync(file.path, 'utf-8').then(data => {
            const fileTitles = markdownImporter.parse(data).content.children.filter(section => {
                return section.level > 0 && section.type === 'hierarchical';
            }).map(title => {
                return { text: title.content, level: title.level, numbering: title.level }
            });
            return { path: file.path, titles: fileTitles };
        }).catch(err => {
            log.error(err);
            throw err;
        });
    });
}

function openBundledFile(filePath) {
    return unZipToTempWorkingDirectory(filePath).then(workingDirectory => {
        return fs.readFileAsync(workingDirectory + "/project.rdg", 'utf8').then(content => {
            const metaRDFFile = JSON.parse(content);
            metaRDFFile.file = {
                path: filePath,
                base: path.parse(filePath).base,
                directory: path.parse(filePath).directory
            };
            metaRDFFile.storage = metaRDFFile.storage || {};
            metaRDFFile.storage.originalPath = filePath;
            metaRDFFile.storage.workingDirectory = workingDirectory;
            metaRDFFile.storage.type = "bundled";
            metaRDFFile.storage.files = [];
            const extension = "." + metaRDFFile.storage.parser;

            return filesWithExtension(workingDirectory, extension).then(filesAndFolders => {
                metaRDFFile.storage.files = filesAndFolders;

                // get titles from all workspace
                const files = filesAndFolders.filter(file => !file.isDirectory);
                return getTitlesFromFiles(files).then(allTitles => {
                    metaRDFFile.allTitles = allTitles;
                    return metaRDFFile;
                });
            });
        });
    });
}

function unZipToTempWorkingDirectory(zipPath) {
    const newWorkingDir = path.join(config.workspaceDir, new Date().getTime().toString());
    return decompress(zipPath, newWorkingDir).then(() => {
        return newWorkingDir
    });
}

function resolveTemplateName(document) {
    return templateService.resolveTemplateName(document.template.name).then(templatePath => {
        document.template.path = templatePath;
    }).catch(e => {
        dialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
            type: "info",
            title: "Téléchargement du modèle",
            message: "Ce document utilise un modèle (plugin) spécifique.\n Rédige va télécharger ce nouveau modèle \"" + document.template.name + "\".",
            buttons: ['Télécharger']
        });
        const spinnerWindow = loadingWindow.initWindow("Téléchargement", "Merci de patienter, Téléchargement du modèle en cours...");
        return templateService.getRemoteTemplate(document.template.name).then(() => {
            spinnerWindow.destroy();
            return resolveTemplateName(document);
        }).catch((err) => {
            spinnerWindow.destroy();
            document.template.path = config.defaultTemplate;
            if (document.template.name !== 'default') {
                dialog.showMessageBox(BrowserWindow.getFocusedWindow(), {
                    type: "info",
                    title: "Le template " + document.template.name + " n'existe pas sur ce poste. ",
                    message: "Le document ouvert utilise maintenant le template par défaut. \nCertaines fonctionnalités ne seront donc pas disponibles.",
                    buttons: ['OK']
                });
            }
        });
    }).then(() => {
        return document;
    });
}

function addTemplateCss(document) {
    const templatePath = path.join(config.templateFolder, document.template.name);
    const templateCss = path.join(templatePath, config.templateCssFile);
    return doesFileExists(templateCss).then(() => {
        document.template.cssPath = templateCss;
    }).catch(e => {
        log.info('No', config.templateCssFile, 'for', document.template.name, 'template');
    }).then(() => {
        return document;
    });
}

function doesFileExists(filePath) {
    return fs.accessAsync(filePath, fs.constants.R_OK);
}

function getRecentFiles() {
    const recents = cache.get('recents') || [];
    return Promise.map(recents, (path) => {
        return doesFileExists(path).then((exists) => ({
            exists: true,
            path: path
        })).catch(() => ({ path: path }));
    }).then((results) => {
        const cleanedList = _.filter(results, { exists: true });
        if (results.length !== cleanedList.length) {
            cache.put('recents', cleanedList);
        }
        return cleanedList;
    });
}

function filesWithExtension(dir, extension) {
    return new Promise((resolve, reject) => {
        const items = [];
        klaw(dir).on('data', item => {
            if (item.stats.isDirectory() || item.path.endsWith(extension)) {
                items.push({ path: item.path, isDirectory: item.stats.isDirectory() });
            }
        }).on('error', (err) => {
            reject(err);
        }).on('end', () => {
            resolve(items);
        });
    });
}

function getRDGFromArgs(aPath) {
    const absolutePath = path.resolve(process.cwd(), aPath || '');
    let files;
    try {
        if (fs.statSync(absolutePath).isDirectory()) {
            files = fs.readdirSync(absolutePath);
        } else {
            files = [absolutePath];
        }
        return files.filter((fileName) => fileName.endsWith('.rdg')).map((name) => path.join(absolutePath, name)).pop();
    } catch (error) {
        console.error("No such file : " + absolutePath);
        return null;
    }
}

module.exports = {
    open: open,
    openRdgCallback: openRdgCallback,
    openNoDialog: openNoDialog,
    doesFileExists: doesFileExists,
    getRecentFiles: getRecentFiles,
    addToRecents: addToRecents,
    getRDGFromArgs: getRDGFromArgs
};
