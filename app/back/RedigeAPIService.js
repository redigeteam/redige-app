'use strict';
const restling = require('restling');
const config = require('../config');
const log = require('electron-log');

let service = {
    publish: function (data) {
        return restling.putJson(config.api + "/documents/" + data.id, data).then(function (e) {
            console.log(e);
        }).catch(function (e) {
            console.log(e);
        });
    }
};

module.exports = service;
