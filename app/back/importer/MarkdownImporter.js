(function () {
    'use strict';
    const marked = require('marked');
    marked.setOptions({
        gfm: true,
        tables: false,
        breaks: false
    });
    const HTMLParser = require('redige-html-parser');

    const service = {
        parse: function (document) {
            return toDocument(prepareToParse(marked.parse(document)));
        }
    };

    function prepareToParse(html) {
        return '<body>' + html.replace(/<\/(h\d|p|ol|ul|pre|blockquote)>\r?\n/gmi, "<\/$1>")
            .replace(/<pre><code>/gmi, '<pre>')
            .replace(/\n(?!<\/div)/g, '<br/>')
            .replace(/<\/code><\/pre>/gmi, '</pre>') + '</body>';
    }

    function toDocument(htmlString) {
        let parser = new HTMLParser({ allowDiv: true });
        return parser.parse(htmlString);
    }

    module.exports = service;
})();