'use strict';

const jsonfile = require('jsonfile');
const mkdirp = require('mkdirp');
const os = require('os');
const path = require('path');
const _ = require('lodash');
const config = require('../config');
const unlink = require('bluebird').promisify(require("fs").unlink);

const cacheFile = path.join(config.redigeTmp, 'redigeCache.json');

let cache = {};
const debouncedSave = _.debounce(saveCache, 100);

function put(key, value) {
    cache[key] = value;
    debouncedSave();
}

function get (key) {
    return cache[key];
}

function loadCache() {
    try {
        cache = jsonfile.readFileSync(cacheFile, 'utf8');
    } catch (err) {
        cache = { defaultPath: os.homedir() };
    }
    return cache;
}

function saveCache() {
    try {
        mkdirp.sync(path.dirname(cacheFile));
        jsonfile.writeFileSync(cacheFile, cache);
    } catch (e) {
        //nop
    }
}

function clearCache() {
    return unlink(cacheFile).catch(() => {
    });
}

module.exports = {
    put: put,
    get: get,
    loadCache: loadCache,
    clearCache: clearCache
};
