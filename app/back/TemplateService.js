const fs = require('fs');
const path = require('path');
const http = require('http');
const https = require('https');
const decompress = require('decompress');
const config = require('../config');

function getRemoteTemplate(templateUrl) {

    if (!templateUrl.startsWith('http')) {
        templateUrl = "https://app.redige.net/api/templates/" + templateUrl;
    }
    const templateName = path.parse(templateUrl).name;
    const saveToPath = path.join(config.templateFolder, templateName + '.zip');

    return downloadTemplate(templateUrl, saveToPath).then(() => {
        return unzipTemplate(saveToPath, templateName);
    });
}

function unzipTemplate(filePath, templateName) {
    return decompress(filePath, path.join(config.templateFolder)).then(() => templateName);
}

function downloadTemplate(url, saveToPath) {
    return new Promise((resolve, reject) => {
        let protocol = http;
        if (url.startsWith('https')) {
            protocol = https;
        }

        protocol.get(url, response => {
            if (response.statusCode === 404) {
                fs.unlink(saveToPath);
                reject({ 'downloadErrorMessage': 'Template introuvable.' });
            } else {
                const file = fs.createWriteStream(saveToPath);
                response.pipe(file);
                file.on('finish', () => {
                    resolve();
                });
            }
        }).on('error', err => {
            fs.unlink(saveToPath);
            reject(err);
        }).setTimeout(60000, () => {
            fs.unlink(saveToPath);
            reject({ 'downloadErrorMessage': 'Abandon du téléchargement du template, délai de 60 secondes dépassé.' });
        });
    });
}

function resolveTemplateName(templateName) {
    const templatePath = path.join(config.templateFolder, templateName);
    return doesFileExists(templatePath).then(() => {
        return templatePath;
    }).catch(e => {
        throw "Template non trouvé : \"" + templateName + "\".";
    });
}

function doesFileExists(filePath) {
    return fs.accessAsync(filePath, fs.constants.R_OK);
}

module.exports = {
    getRemoteTemplate: getRemoteTemplate,
    resolveTemplateName: resolveTemplateName
};
