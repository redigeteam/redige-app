const fs = require('fs');
const path = require('path');
const log = require('electron-log');
const config = require('../../config.js');
const Promise = require('bluebird');
const readFile = Promise.promisify(require("fs").readFile);
const writeFile = Promise.promisify(require("fs").writeFile);
const HtmlExporter = require('redige-html-exporter');
const ExporterUtils = require('./ExporterUtils');

let documentFolder;

module.exports = {
    toRevealFormat: function (path, document, asPDF) {
        return new Promise(function (resolve, reject) {
            documentFolder = document.file.workingDirectory;
            handleDocument(path, document, asPDF);
            resolve();
        });
    }
};

let HTML_TAGS = {
    '1': 'h1',
    '2': 'h2',
    '3': 'h3',
    '4': 'h4',
    '5': 'h5',
    '6': 'h6'
};

function handleDocument(path, document, asPDF) {

    getTemplateCss(document.template.name)
        .catch(error => {
            log.info('No custom css for Reveal.js export found');
            return '';
        })
        .then(templateCss => {
            const pdfCss = asPDF ? '<link rel="stylesheet" href="css/print/pdf.css">' : '';
            const html = '<html>' +
                '<head>' +
                '<meta charset="utf-8" />' +
                '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">' +
                '<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>' +
                '<link rel="shortcut icon" type="image/png" href="favicon-32x32.png" sizes="32x32"/>' +
                '<link rel="shortcut icon" type="image/png" href="favicon-16x16.png" sizes="16x16"/>' +
                '<link rel="stylesheet" href="css/reveal.css">' +
                '<link rel="stylesheet" href="lib/css/zenburn.css">' +
                pdfCss +
                '<style type="text/css">' + templateCss + '</style>' +
                '<script src="lib/js/head.min.js"></script>' +
                '<script src="js/reveal.js"></script>' +
                '</head>' +
                '<body>' +
                '<div class="reveal">' +
                '<div class="slides">' + handleDocumentContent(document) +
                '</section></section>' + // closing last section
                '</div>' +
                '</div>' +
                '<script src="plugin/notes/notes.js"></script>' +
                '<script src="run.js"></script>' +
                '</body>' +
                '</html>';
            return writeFile(path, html);
        })
        .catch(error => {
            console.error(error);
        });
}

function getTemplateCss(templateName) {
    const cssTemplatePath = path.join(config.templateFolder, templateName, 'reveal.css');
    return readFile(cssTemplatePath, 'utf8').then(function (css) {
        return css;
    });
}

function handleDocumentContent(document) {
    let result = '';
    let first = { isFirst: true };

    let firstListItem = true;
    let sectionsList = [];
    document.children.forEach(docChild => {
        if (!firstListItem && !isList(docChild)) {
            // if end of a list
            result += HtmlExporter.convertListItem(sectionsList);
            firstListItem = true;
        }

        if (docChild.children) {
            result += handleDivision(docChild, first);
        } else if (isList(docChild)) {
            sectionsList.push(docChild);
            firstListItem = false;
        } else {
            result += handleSection(docChild, first);
        }
    });
    return result;
}

function handleDivision(division, first) {
    let result = '';
    let endTag = '';
    if (division.type === 'blockquote') {
        result += '<pre><blockquote data-trim data-noescape>';
        endTag = '</blockquote></pre>';
    } else if (division.type === 'blockcode') {
        result += '<pre class="reveal"><code data-trim data-noescape>';
        endTag = '</code></pre>';
    } else if (division.type === 'blocknote') {
        result += '<aside class="notes">';
        endTag = '</aside>';
    }
    division.children.forEach(function (section) {
        result += handleSection(section, first);
    });
    return result + endTag;
}

function handleSection(section, first) {
    let result = '';
    if (section.type === "attachment") {
        result += '<p class="attachment"></p>';
    } else if (section.type === 'image') {
        result += '<img src="' + ExporterUtils.resolveImagePath(section.data, documentFolder) + '"></img>';
    } else {
        // create slides and sub-slides
        if (isRevealSection(section)) {
            result += createSlide(section, first);
        }

        // define HTML tag
        let sectionTag = 'p';
        if (isTitle(section)) {
            sectionTag = HTML_TAGS[section.level];
        }
        // add attributes to tag
        result += '<' + sectionTag + ' class="' + section.type + ' ' + section.type + "_" + section.level;
        if (section.numbering) {
            result += '" numbering="' + section.numbering;
        }
        result += '">';

        // add ids
        if (section.numbering) {
            result += '<a class="anchor" name="' + section.id + '"></a>';
        }

        result += convertSectionContent(section);
        result += '</' + sectionTag + '>';
    }
    return result;
}

function createSlide(section, first) {
    let result = '';
    if (section.level === 1 && first.isFirst) {
        result += '<section>';
        first.isFirst = false;
    } else if (section.level === 1) {
        result += '</section>';
        result += '</section>';
        result += '<section>';
    } else if (section.level === 2) {
        result += '</section>';
    }
    result += '<section>';

    return result;
}

function convertSectionContent(section) {
    let content = "";
    let spanOpenned = [];
    if (section.content !== undefined) {
        for (let index = 0; index < section.content.length; index++) {
            if (!spanOpenned.length || isDecoratorChange(section, index)) {
                content += changeSpan(index, section);
            }
            content += HtmlExporter.htmlEncode(section.content.substring(index, index + 1));
        }
        if (spanOpenned.length) {
            content += '</' + spanOpenned.pop() + '>';
        }
    }

    return content;

    function changeSpan(offset, section) {
        let result = '';
        if (spanOpenned.length) {
            result = '</' + spanOpenned.pop() + '>';
        }
        let link = getLink(section, offset);
        if (link != null) {
            result += '<a href="' + link.url + '" ';
            spanOpenned.push("a");
        } else {
            result += '<span ';
            spanOpenned.push("span");
        }
        let classList = buildDecoratorClass(section, offset);
        if (classList.length > 0) {
            result += 'class="' + classList + '"';
        }
        result += '>';

        return result;
    }
}

function buildDecoratorClass(section, offset) {
    let result = '';
    section.decorators.forEach(function (decorator) {
        if (offset >= decorator.start && offset < decorator.end) {
            if (result.length > 0) {
                result += " ";
            }
            result += decorator.type;
        }
    });
    return result;
}

function getLink(section, offset) {
    return section.decorators.find(function (decorator) {
        return decorator.type === 'link' && (offset >= decorator.start && offset < decorator.end);
    });
}

function isDecoratorChange(section, offset) {
    return section.decorators.filter(function (decorator) {
        return (offset == decorator.start || offset == decorator.end);
    }).length !== 0;
}

function isRevealSection(section) {
    return (section.level === 1 || section.level === 2) && isTitle(section);
}

function isTitle(section) {
    return section.level >= 1 && (section.type === 'hierarchical' || section.numbering !== null);
}

function isList(section) {
    return isUlist(section) || isOlist(section);
}

function isUlist(section) {
    return section.type === 'ulist';
}

function isOlist(section) {
    return section.type === 'olist';
}
