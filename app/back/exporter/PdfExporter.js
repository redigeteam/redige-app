const HTMLExporter = require('./HTMLExporter');
const config = require("../../config");
const fs = require("fs");
const Promise = require("bluebird");
const htmlToPdf = require('html-to-pdf');

htmlToPdf.setInputEncoding('UTF-8');
htmlToPdf.setOutputEncoding('UTF-8');
htmlToPdf.setDebug(config.env === 'DEV');

function toPDF(pdfPath, tmpPath) {
    return new Promise((resolve, reject) => {
        const readStream = fs.createReadStream(tmpPath);
        let html = '';
        readStream.on('data', chunk => {
            html += chunk;
        }).on('end', () => {
            htmlToPdf.convertHTMLString(html, pdfPath, (error, success) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(success);
                    }
                }
            );
        });
    });
}

module.exports = {
    exportToPDF: function (pdfPath, children, fileDirectory, templateName) {
        return HTMLExporter.convert(children, fileDirectory, templateName).then(tmpPath => {
            return toPDF(pdfPath, tmpPath);
        });
    }
};
