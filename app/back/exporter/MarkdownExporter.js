(function () {
    'use strict';
    const _ = require('lodash');
    const service = {
        convert: function (document) {
            return handleDocumentContent(document);
        }
    };

    const TITLE_TAGS = {
        '0': '',
        '1': '# ',
        '2': '## ',
        '3': '### ',
        '4': '#### ',
        '5': '##### ',
        '6': '###### '
    };

    const START_STYLE_TAGS = {
        'code': '`',
        'bold': '**',
        'italic': '_',
        'link': '['
    };

    const END_STYLE_TAGS = {
        'code': '`',
        'bold': '**',
        'italic': '_',
        'link': ']'
    };

    function handleDocumentContent(document) {
        let result = '';
        let previousSection = null;
        document.children.forEach(function (docChild) {
            if (docChild.children) {
                result += handleDivision(docChild);
            } else {
                result += handleSection(docChild, '', false, previousSection);
            }
            previousSection = docChild;
        });
        return result;
    }

    function handleDivision(division) {
        let result = '';
        let linePrefix = '';
        let endTag = '';
        if (division.type === 'blockquote') {
            result += '\n';
            linePrefix = "> ";
            endTag = '\n\n';
        } else if (division.type === 'blockcode') {
            result += '\n```\n';
            endTag = '```\n\n';
        } else {
            result += '\n\n<div class="' + division.type + '">\n';
            endTag = '</div>\n\n';
        }
        let previousSection = null;
        division.children.forEach(function (section) {
            result += handleSection(section, linePrefix, division.type === 'blockcode', previousSection);
            previousSection = section;
        });
        return result + endTag;
    }

    function handleSection(section, linePrefix, isCode, previousSection) {
        if (!_.isUndefined(section.content) && section.content.length === 0) {
            return '';
        }
        let result = linePrefix || '';
        if (section.type === "attachment") {
            result += '';
        } else if (section.type === 'image') {
            result += '\n' + linePrefix + '![](' + section.data + ')\n';
        } else if (section.type === 'olist') {
            if (previousSection && previousSection.type === "ulist" && previousSection.level === section.level) {
                result += "\n" + linePrefix + "\n" + linePrefix;
            }
            result += getListIndent(section) + '1. ' + convertSectionContent(section) + '\n';
        } else if (section.type === 'ulist') {
            if (previousSection && previousSection.type === "olist" && previousSection.level === section.level) {
                result += "\n" + linePrefix + "\n" + linePrefix;
            }
            result += getListIndent(section) + '- ' + convertSectionContent(section) + '\n';
        } else {
            if (!isCode) {
                if (previousSection && (previousSection.type === "ulist" || previousSection.type === "olist")) {
                    result += "\n" + linePrefix + "\n" + linePrefix;
                }
            }
            result += TITLE_TAGS[section.level] + convertSectionContent(section) + '\n';
        }
        return result;
    }

    function getListIndent(section) {
        let result = '';
        for (let index = 0; index < section.level; index++) {
            result += '  ';
        }
        return result;
    }

    function convertSectionContent(section) {
        let content = "";
        for (let index = 0; index <= section.content.length; index++) {
            if (!content.length || isDecoratorChange(section, index)) {
                content += changeSpan(index, section);
            }
            content += section.content.substring(index, index + 1);
        }
        return content;

        function changeSpan(offset, section) {
            let result = '';
            section.decorators.filter(function (decorator) {
                return offset == decorator.end;
            }).forEach(function (decorator) {
                result += getEndSyntax(decorator);
            });

            section.decorators.filter(function (decorator) {
                return offset == decorator.start;
            }).forEach(function (decorator) {
                result += getStartSyntax(decorator);
            });

            return result;
        }
    }

    function getStartSyntax(decorator) {
        return START_STYLE_TAGS[decorator.type];
    }

    function getEndSyntax(decorator) {
        let result = END_STYLE_TAGS[decorator.type];
        if (decorator.type === 'link') {
            result += '(' + decorator.data + ')';
        }
        return result;
    }

    function isDecoratorChange(section, offset) {
        return section.decorators.filter(function (decorator) {
            return (offset == decorator.start || offset == decorator.end);
        }).length !== 0;
    }

    module.exports = service;
})();