const path = require('path');

function resolveImagePath(imagePath, documentFolder) {
    if (imagePath.toLowerCase().match(/^(?!\.\.)(?!\/)(.\/)?.*\.(jpeg|jpg|gif|png|svg)$/i) !== null) {
        return path.join(documentFolder, imagePath);
    } else {
        return imagePath
    }
}

module.exports = {
    resolveImagePath: resolveImagePath
};
