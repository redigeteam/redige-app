const config = require('../../config.js');
const Epub = require("epub-gen");
const HtmlExporter = require('redige-html-exporter');
const path = require('path');
const readFile = require('bluebird').promisify(require("fs").readFile);

module.exports = {
    toEpub: function (document) {
        return getEpubCss(document.template.name).then(css => {
            return handleDocument(document, css);
        });
    }
};

function getEpubCss(templateName) {
    const epubCssPath = path.join(config.templateFolder, templateName, 'epub.css');
    return readFile(epubCssPath, 'utf8').then(css => {
        return css;
    });
}

function handleDocument(document, css) {
    const pathInfo = path.parse(document.file.parentPath);

    const htmlWithCommas = HtmlExporter.exportDocument(document).replace(/<h1>/gi, ',<splitHere><h1>');
    const htmlRelativeImages = htmlWithCommas.replace(/<img src=".\//gi, '<img src="file://' + document.file.workingDirectory + '/');
    const chapters = htmlRelativeImages.split('<splitHere>');
    chapters.shift(); // remove first empty element

    const options = {
        title: pathInfo.name,
        author: '?AUTHOR?',
        css: css,
        content: chapters.map(chapter => {
            return {
                title: /<h1>(.*?)<\/h1>/g.exec(chapter)[1],
                data: chapter
            };
        })
    };

    const epubPath = path.join(pathInfo.dir, pathInfo.name) + '.epub';
    return new Epub(options, epubPath).promise.then(() => {
        return epubPath;
    });
}
