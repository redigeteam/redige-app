const Promise = require('bluebird');
const readFile = Promise.promisify(require("fs").readFile);
const writeFile = Promise.promisify(require("fs").writeFile);
const fs = require('fs');
const os = require('os');
const path = require('path');
const config = require('../../config.js');
const ExporterUtils = require('./ExporterUtils');

let documentFolder;

const service = {
    convert: function (children, fileDirectory, templateName) {
        documentFolder = fileDirectory;
        return handleDocument(children, templateName);
    }
};

let HTML_TAGS = {
    '0': 'p',
    '1': 'h1',
    '2': 'h2',
    '3': 'h3',
    '4': 'h4',
    '5': 'h5',
    '6': 'h6'
};

let bookmarks = '';

function handleDocumentContent(children) {
    let result = '';
    children.forEach(function (docChild) {
        if (docChild.children) {
            result += handleDivision(docChild);
        } else {
            result += handleSection(docChild);
        }
    });
    return result;
}

function handleDivision(division) {
    let result = '';
    let endTag = '';
    if (division.type === 'blockquote') {
        result += '<div class="blockquote">';
        endTag = '</div>';
    } else if (division.type === 'blockcode') {
        result += '<pre><code>';
        endTag = '</code></pre>';
    } else {
        result += '<div class="' + division.type + '">';
        endTag = '</div>';
    }
    division.children.forEach(function (section) {
        result += handleSection(section);
    });
    return result + endTag;
}

function handleSection(section) {
    let result = '';
    if (section.type === "attachment") {
        result += '<div class="attachment"></div>';
    } else if (section.type === 'image') {
        result += '<img src="' + ExporterUtils.resolveImagePath(section.data, documentFolder) + '"></img>';
    } else {
        let sectionTag = HTML_TAGS[section.level];
        if (section.type === 'olist' || section.type === 'ulist') {
            sectionTag = 'p';
        }

        if (section.numbering) {
            result += '<' + sectionTag + ' class="' + section.type + ' ' + section.type + "_" + section.level + '" numbering="' + section.numbering + '">';
        } else {
            result += '<' + sectionTag + ' class="' + section.type + ' ' + section.type + "_" + section.level + '">';
        }

        if (section.numbering) {
            result += '<a class="anchor" name="' + section.id + '"></a>';
        }
        result += convertSectionContent(section);
        result += '</' + sectionTag + '>';
    }
    return result;
}

function convertSectionContent(section) {
    let content = "";
    let spanOpenned = [];
    for (let index = 0; index < section.content.length; index++) {
        if (!spanOpenned.length || isDecoratorChange(section, index)) {
            content += changeSpan(index, section);
        }
        content += section.content.substring(index, index + 1);
    }
    if (spanOpenned.length) {
        content += '</' + spanOpenned.pop() + '>';
    }

    return content;

    function changeSpan(offset, section) {
        let result = '';
        if (spanOpenned.length) {
            result = '</' + spanOpenned.pop() + '>';
        }
        let link = getLink(section, offset);
        if (link != null) {
            result += '<a href="' + link.url + '" ';
            spanOpenned.push("a");
        } else {
            result += '<span ';
            spanOpenned.push("span");
        }
        let classList = buildDecoratorClass(section, offset);
        if (classList.length > 0) {
            result += 'class="' + classList + '"';
        }
        result += '>';

        return result;
    }
}

function buildDecoratorClass(section, offset) {
    let result = '';
    section.decorators.forEach(function (decorator) {
        if (offset >= decorator.start && offset < decorator.end) {
            if (result.length > 0) {
                result += " ";
            }
            result += decorator.type;
        }
    });
    return result;
}

function getLink(section, offset) {
    return section.decorators.find(function (decorator) {
        return decorator.type === 'link' && (offset >= decorator.start && offset < decorator.end);
    });
}

function isDecoratorChange(section, offset) {
    return section.decorators.filter(function (decorator) {
            return (offset == decorator.start || offset == decorator.end);
        }).length !== 0;
}

function getTemplateCss(templateName) {
    const cssTemplatePath = path.join(config.templateFolder, templateName, '/pdf.css');
    return readFile(cssTemplatePath, 'utf8').then(function (css) {
        return css;
    });
}

function handleDocument(children, templateName) {
    let currentTemplate = config.emptyDefaultTemplate.template.name;
    if (templateName !== null) {
        currentTemplate = templateName;
    }

    return getTemplateCss(currentTemplate).then(templateCss => {
        const content = handleDocumentContent(children);
        const html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">' +
            '<html xmlns="http://www.w3.org/1999/xhtml">' +
            '<head>' +
            '<meta name="generator" content="Redige" />' +
            '<meta charset="utf-8" />' +
            '<style type="text/css">' +
            templateCss +
            '</style>' +
            '</head>' +
            '<body>' +
            '<div class="page">' +
            '<div class="content">' + content +
            '</div>' +
            '</div>' +
            '</body>' +
            '</html>';

        const tmpPath = path.join(os.tmpdir(), 'redigeDoc.html');
        return writeFile(tmpPath, html).then(() => tmpPath);
    });
}

module.exports = service;
