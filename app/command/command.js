const yargs = require('yargs');
const appVersion = require('../package.json').version;

//in packaging mode, the argv include unwanted prefix
if (!process.defaultApp) {
    yargs.parse(process.argv.slice(1))
}

yargs.version(function () {
    return appVersion;
});

yargs.usage('$0 [options] path')
    .option('h', {
        alias: 'headless',
        default: false,
        describe: 'headless mode'
    })
    .command({
        command: 'help',
        desc: 'Show help',
        handler: () => {
            yargs.showHelp();
        }
    })
    .help(false);

module.exports = {
    headless: yargs.argv.headless,
    path: yargs.argv._[0],
};