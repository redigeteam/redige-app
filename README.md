# Rédige

## Template API

You can define a template to customize packaging, the toolbar and exports styles (PDF, Reveal.js...).
A template always contains a JSON file called `index.js` following this structure :

```javascript
module.exports = {
    toolbar: {
        exports: ['pdf',
			{
        		type: 'reveal',
        		icon: './image.png',
                tooltip: 'Reveal.js',
                format: 'reveal',
                process: function () {
                	electron.exportReveal();
                }
        	}
		],
        inlines: ['code', 'quote',
            //Example of a custom element API
            {
                type: 'strike',
                tooltip: 'Barré',
                icon: './image.jpg',
                process: function () {
                    var currentSelection = Redige.getDocumentSelection();
                    Redige.patch({
                        action: 'toggleDecorator',
                        sectionId: currentSelection.startSection.id,
                        decorator: {
                            start: currentSelection.startOffset,
                            end: currentSelection.endOffset,
                            type: 'strike'
                        }
                    });
                }
            }
        ],
        sections: ['normal', 'header', 'olist', 'ulist'],
        divisions: ['blockquote', 'blockcode'],
        inserts: ['image', 'attachment']
    },
    language: 'FR',
};
```
```css
 /* editor.css */
 .strike {
   text-decoration: line-through;
 }
```
### 1. Toolbar
The toolbar contains 5 types of buttons : `exports`, `inlines`, `sections`, `divisions` and `inserts`.

#### 1.1. Exports
`Exports` parse the current document and export it to a new format.  
Available exports are : `pdf`, `reveal.js`

#### 1.2. Inlines
`Inlines` apply a specific style to the current selection.  
Default inlines are : `code`, `important`, `quote`, `link`.  
Custom inlines can be defined in the `index.js` file of the the template.  
The `strike` inline in the example above illustrates how you can create custom inlines.  

#### 1.3. Sections
`Sections` are types of content.  
Available sections are : `normal`, `header`, `olist`, `ulist`

#### 1.4. Divisions
`Divisions` are used to represent multiple paragraphs of a type (code, quote...)
Available divisions are : `blockquote`, `blockcode`

#### 1.5. Inserts
`Inserts` are used to display files in the document.  
Available inserts are : `image`, `attachment`

### 2. Packaging
Each Redige document has a `template` property which defines available features and the visual aspect of the editor.  
Those templates are saved in the hidden folder `.redige/templates` which is placed in the home directory of the user.  
This folder also contains a file called called `custom.css`. This file can add CSS rules to the editor.  

### 3. Template styles
If the template allows exports to PDF, a `pdf.css` file can define the style for the exported document.  
You can also create a `reveal.css` file to add CSS rules to your Reveal.js export.  
If you want to add template specific CSS rules to your editor, you can create a `editor.css` file.  

Those files must be placed in the `.redige/templates/<template>` folder.



